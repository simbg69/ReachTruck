#include "PID.h"

int do_PID(int val, int target){
    int error = target - val;
    int d_error = error - p_error;
    int ret_val = 0;
    
    p_error = error;
    
    long p_term = (long)(EEDATA[KP] * error);
    long i_term = (long)(EEDATA[KI] * i_error);
    long d_term = (long)(EEDATA[KD] * d_error);
    ret_val = (long)((p_term + i_term - d_term))/64;
    if(ret_val > EEDATA[PID_MAX]){
        ret_val = EEDATA[PID_MAX];
    }
    else if (ret_val < EEDATA[PID_MIN]){
        ret_val = EEDATA[PID_MIN];
    }
    else{
        i_error += error;
    }
    
    return ret_val;
}

void reset_PID(){
    i_error = 0;
    p_error = 0;
}
