/**
  Company:
    Aitech Robotics & Automation.

  File Name:
    main.c

  Summary:
    This code is for Traction Wheel used in Auto Stacker Project
 * ERROR CODE:
 * BIT 1 - Encoder Failed
 * BIT 0 - Bumper Engaged

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB(c) Code Configurator - 3.16
        Device            :  PIC16F1716
        Driver Version    :  2.00
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.35
        MPLAB             :  MPLAB X 3.20
*/

/*
    (c) 2017 Aitech Robotics & Automation. and its subsidiaries. You may use this
    software and any derivatives exclusively with Aitech products.
*/
#include "mcc_generated_files/mcc.h"
#include "bareMetal.h"
#include "PID.h"

#define MY_ADDR 0x1
#define MY_ADDR2 0
#define MOVE_TIMEOUT_TICK 15

CAN_Data Report_Data;

int *current_dac;
uint8_t *Status;

/*FLAGS*/
bool isMoving=0;
bool ignoreBumper = 0;
bool en_PID = false;
bool autonomous_mode = true;
int8_t move_timeout = 0; //0 = timeout 

uint8_t enc_timeout_cnt = 0;
int *mvel = 0,UpCount = 0,DnCount = 0;
int target_vel = 0;
int prev_target_vel = 0;

long encoder_count = 0;
int *encoder_count_L = 0;
void Bumper_Check();
void Moving(int dac_val);
void Set_Enc(long val);
void reset_wheel_pid();

/*Interrupt*/
void TMR0_calback();
void device_secific_command(CAN_Data buff);
/*Threads*/
uint8_t ID_RP_Wheel;
void RP_Wheel();
void toggle_buzzer();
void read_encoder();

//void check_encoder();

void putStr(char *txStr)
{
    while (*txStr != 0)
    {
        putch(*txStr);
        txStr++;
    }
}

void uartPrintVersion()
{
    char version[16];
    sprintf(version, "%s", "Wheel v0.4.0\n\r");
    putStr(version);
}

/*TODO:-
 * 
   2. Restructure Report Message
 * 3. Multiple Address
 */
void main(void)
{
    // initialize the device
    SYSTEM_Initialize();
    init_bareMetal(MY_ADDR,MY_ADDR2);
    Buzzer_SetLow();
    Report_data_ptr = &Report_Data;
    encoder_count_L = &Report_Data.data[2];
    mvel = &Report_Data.data[4];
    current_dac = &Report_Data.data[6];
    Status = &Report_Data.data[1];
    Report_Data.len = 8;
    Report_Data.node_addr = MY_ADDR;
    
    SetCommandCallBack(device_secific_command);
    
    DAC1_SetOutput(0);
    EN_SetHigh();
    
    register_thread(20,read_encoder);
    register_thread(500,toggle_buzzer);
    //register_thread(500,check_encoder);

    TMR0_SetInterruptHandler(TMR0_calback);

    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();
    SWDTEN = 1;
    
    //uartPrintVersion();
    
    while (1)
    {
        thread_update();
        Received_Command();
        Bumper_Check();
        
        //uartPrintVersion();
    }
}

void read_encoder(){
    uint8_t BumperCondition = Omron2_GetValue() << 1 | Omron1_GetValue();
    bool rear_laser = ignoreBumper==1?0:Omron2_GetValue();
    int tmp_vel = 0;
    
    
    if((autonomous_mode == true) && (move_timeout > 0))
    {
        move_timeout--;
        if(move_timeout == 0)
        {
            EN_SetHigh();
            DAC1_SetOutput(0);
            reset_wheel_pid();
        }
    }
    
    *mvel = DnCount;
    *mvel -= UpCount; 
   	
    DnCount = (TMR0_H<<8)|TMR0_ReadTimer();
    UpCount = TMR1_ReadTimer();

    *mvel += UpCount;
    *mvel -= DnCount;

    if(EEDATA[ENC_POLARITY] == 0)
    {   
        if(EEDATA[DIRECTION] == 0)
        {
            encoder_count += (long)*mvel;
        }
        else
        {
            encoder_count -= (long)*mvel;
        }
    }
    else
    {
        if(EEDATA[DIRECTION] == 1)
        {
            encoder_count -= (long)*mvel;
        }
        else
        {
            encoder_count += (long)*mvel;
        }
    }
    
    *encoder_count_L = (int)encoder_count;
    isMoving = *mvel==0?false:true;
    
    if (en_PID){
        if (Omron1_GetValue()== 1 || rear_laser){
            reset_wheel_pid();
            tmp_vel = 0;
            prev_target_vel=0;
        } 
        else {
            if (target_vel > (prev_target_vel + EEDATA[RAMP]))
                tmp_vel = prev_target_vel + EEDATA[RAMP];
            else
                tmp_vel = target_vel;
        }
        Moving(do_PID(*mvel*8,tmp_vel));
        prev_target_vel = tmp_vel;
    }
        *current_dac = REV_GetValue()== 0? (DAC1CON1):-(DAC1CON1);
        *Status = (BumperCondition &3)<<1| isMoving;
}

void Moving(int dac_val){
    
    if ((autonomous_mode == true) && (move_timeout == 0))
    {
        EN_SetHigh();
        //FWD_SetLow();
        //REV_SetLow();
        DAC1_SetOutput(0);
        reset_wheel_pid();
        return;
    }
    if (error_code == 0){
        if(dac_val>0){
            EN_SetLow();
            if(EEDATA[DIRECTION] == 0)
            {
                REV_SetLow();
                FWD_SetHigh();
            }
            else
            {
                REV_SetHigh();
                FWD_SetLow();
            }
            DAC1_SetOutput(dac_val);  
        }
        else if(dac_val<0){
            EN_SetLow();
            if(EEDATA[DIRECTION] == 0)
            {
                FWD_SetLow();
                REV_SetHigh();
            }
            else
            {
                FWD_SetHigh();
                REV_SetLow();
            }
            DAC1_SetOutput((dac_val*-1));  
        } else {
            EN_SetHigh();
            FWD_SetLow();
            REV_SetLow();
            DAC1_SetOutput(0);
            reset_wheel_pid();
        }
    }
    else{
        EN_SetHigh();
        FWD_SetLow();
        REV_SetLow();
        DAC1_SetOutput(0);
    }
}

void Set_Enc(long Target_encoder){
    read_encoder();
    __delay_ms(500);
    read_encoder();
    TMR0_H =0;
    TMR0_Reload();
    TMR1_Reload();
    encoder_count=Target_encoder;
}

void TMR0_calback(void){
    TMR0_H++;
}

void toggle_buzzer(){
    if (isMoving ==1){
        Buzzer_Toggle();
    } else{
        Buzzer_SetLow();
    }
}

void Bumper_Check(){
    bool rear_sensor = ignoreBumper==1?0:Omron2_GetValue();
    if (Omron1_GetValue()==1 || rear_sensor ){
        error_code |= 1;
        reset_wheel_pid();
        EN_SetHigh();
    }
    else
        error_code &= 0xFE;
}

/*void check_encoder(){
    static long prev_enc = 0;
    static uint8_t counter = 0;
    
    //EN is active LOW
    if(EN_GetValue()==0){
        if((FWD_GetValue()==1 || REV_GetValue()==1)&& DAC1CON1 >0){
            if (prev_enc == encoder_count){
                counter=(counter>=uint8_t(EEDATA[ENC_TIMEOUT]))?uint8_t(EEDATA[ENC_TIMEOUT]):counter+1;
            } else {
                counter = 0;
            }
            if(counter >= uint8_t(EEDATA[ENC_TIMEOUT])){
                 EN_SetHigh();
                 reset_wheel_pid();
                 error_code |= 0x2;
            } else
                error_code &= 0xFD;
        }
    }
    else{
        counter = 0;
    }
    prev_enc = encoder_count;
}*/

void reset_wheel_pid(){
    reset_PID();
    prev_target_vel= 0;
}

void device_secific_command(CAN_Data buff){
    switch(buff.data[0]){
        case 0: //DAC Move
            move_timeout = MOVE_TIMEOUT_TICK;
            Moving((buff.data[1]<<8)|buff.data[2]);
            break;
        case 1:  //Stop
            move_timeout = MOVE_TIMEOUT_TICK;
            en_PID = false;
            reset_wheel_pid();
            Moving(0);
            break;
        case 2: //Set Encoder
            Set_Enc(((long)(buff.data[1])<<24)|((long)(buff.data[2])<<16)|((long)(buff.data[3])<<8)|(long)(buff.data[4]));
            break;
        case 4: //Move with PID
            move_timeout = MOVE_TIMEOUT_TICK;
            target_vel =(int)(buff.data[1]<<8|buff.data[2]);
            en_PID = true;
            break;
        case 5:
            ignoreBumper = buff.data[1];
            break; 
        case 6:
            autonomous_mode = true;
            break; 
        case 7:
            autonomous_mode = false;
            break; 
    }
}