/* 
 * File:   PID.h
 * Author: Jonathan
 *
 * Created on 23 December, 2016, 9:22 AM
 */

#ifndef PID_H
#define	PID_H

#ifdef	__cplusplus
extern "C" {
#endif
#include <xc.h>
#include "mcc_generated_files/mcc.h"
#include "EEPROM.h"

    long i_error = 0;
    long p_error = 0;
    
    int do_PID(int val, int target);
    void reset_PID();


#ifdef	__cplusplus
}
#endif

#endif	/* PID_H */

