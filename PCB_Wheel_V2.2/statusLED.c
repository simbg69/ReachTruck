#include "statusLED.h"

void status_LED(){
    static uint8_t counter;
    static bool LED_rest = 0;
    bool flip = 0;
    counter++;

    if (LED_rest){
        LED_SetLow();
        if (counter >STATUS_LED_DARK){
            flip = 1;
        }
    } 
    else {
        LED_Toggle();
        if(counter >= LED_status*2){
            flip = 1;
        }
    }
    if (flip){
        counter = 0;
        LED_rest = !LED_rest;
    }
}