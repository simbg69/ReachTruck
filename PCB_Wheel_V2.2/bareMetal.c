/*
 * File:   bareMetal.c
 * Author: Jonathan
 *
 * Created on March 31, 2017, 2:12 PM
 */

#include <xc.h>
#include "bareMetal.h"

void (*DeviceSpecificCommand)(CAN_Data);

void init_bareMetal(uint16_t addr1,uint16_t addr2){
    ADDR = addr1;
    ADDR2 = addr2;
    LED_status = ADDR;  
    scheduler_init();
    cc_reset();
    __delay_ms(ADDR<<3);
    cc_init(CLKOUT_PS4,ADDR,ADDR2);
    
    restore_EEData();
    ID_LED_status =register_thread(LED_PERIOD_NORMAL,status_LED);
    ID_STATUS_REPORT = register_thread(EEDATA[REPORT_PERIOD],status_report); 
    ID_ERR_REPORT = register_thread(1000,err_handler);
    RP_Reset();
    SetCommandCallBack(default_CallBack);
    Report_data_ptr = &default_data;
}

void RP_Reset()
{
    CAN_Data buff1;
    buff1.RTR = 0;
    buff1.node_addr = (1<<10)|ADDR;
    buff1.data[0]= 254;
    buff1.data[1]= STKOVF;
    buff1.data[2]= (STKUNF<<7)|(nRWDT<<6)|(nRMCLR<<5)|(nRI<<4)|(nPOR<<3)|(nBOR<<2)|(nTO<<1)|(nPD);
    buff1.len = 3;
    cc_LoadTx(buff1);
    PCON = 0x1F;
    nTO = 1;
    nPD = 1;
}

void err_handler(){
    CAN_Data buff1;
    if (error_code> 0){
        buff1.RTR = 0;
        buff1.node_addr = (1<<10)|ADDR;
        buff1.data[0]= 0;
        buff1.data[1]= error_code;
        buff1.len = 2;
        cc_LoadTx(buff1);
        LED_status = error_code;
        thread_reschedule(ID_LED_status,LED_PERIOD_ERROR);
    } else{
        LED_status = ADDR;
        thread_reschedule(ID_LED_status,LED_PERIOD_NORMAL);
    }
}

void periodic_report(uint16_t period){
    if (period == 0){
        thread_pause(ID_STATUS_REPORT);
        thread_pause(ID_ERR_REPORT);
    } else{
        thread_reschedule(ID_STATUS_REPORT,period);
        thread_resume(ID_STATUS_REPORT);
        thread_resume(ID_ERR_REPORT);
    }
}

void status_report(){
    static bool toggle = 0;
    static unsigned int counter = 0;
    Report_data_ptr->data[0] = toggle<<7| 2;
    Report_data_ptr->RTR = 0;
    Report_data_ptr->node_addr = (1<<10)|ADDR;
    toggle = ~toggle;
    cc_LoadTx(*Report_data_ptr) ;
}

void Received_Command(){
    CAN_Data buff,buff1;
    static long received_cnt = 0;
    uint8_t buffer = 0xff;
    buffer = get_buffer_ready();
    if (CAN_INT_GetValue() == 0){
        while(buffer)
        {
            CLRWDT();
            if(buffer & 1){
                buff = cc_readRxBn(MCP_READ_RX0);}
            else if(buffer & 2){
                buff = cc_readRxBn(MCP_READ_RX1);} 

            if(buff.node_addr == ADDR || buff.node_addr == 0){
                switch(buff.data[0]){
                    case 128:
                        CLRWDT();
                        break;
                    case 129: //Disable WDT
                        SWDTEN = 0;
                        break;
                    case 130:
                        periodic_report(buff.data[1]<<8|buff.data[2]);
                        break;
                    case 190:
                        for (int i = 0; i< MAX_THREAD; i++){
                            thread_pause(i);
                        }
                        switch(buff.data[1]){
                            case 1:
                                LATBbits.LATB7 = buff.data[2];
                                break;
                            case 2:
                                LATBbits.LATB6 = buff.data[2];
                                break;
                            case 3:
                                LATCbits.LATC1 = buff.data[2];
                                break;
                            case 4:
                                LATBbits.LATB1 = buff.data[2];
                                break;
                            case 5:
                                LATAbits.LATA3 = buff.data[2];
                                break;
                            case 6:
                                LATAbits.LATA5 = buff.data[2];
                                break;
                            case 7:
                                LATAbits.LATA7 = buff.data[2];
                                break;
                            case 8:
                                LATAbits.LATA6 = buff.data[2];
                                break;
                            case 9:
                                LED_Toggle();
                                break;
                        }
                        break;
                    case 191:
                        for (int i = 0; i< MAX_THREAD; i++){
                            thread_pause(i);
                        }
                        char IO = PORTBbits.RB7|PORTBbits.RB6<<1| PORTCbits.RC1<<2| \
                            PORTBbits.RB1<<3|PORTAbits.RA3<<4| PORTAbits.RA5<<5| \
                            PORTAbits.RA7<<6 | PORTAbits.RA6<<7;
                        buff1.RTR = 0;
                        buff1.node_addr =(1<<10)|ADDR;
                        buff1.data[0] = 0xFD;
                        buff1.data[1] = IO;
                        buff1.len = 2;
                        cc_LoadTx(buff1);
                        break;
                    case 200:
                        buff1.RTR = 0;
                        buff1.node_addr =(1<<10)|ADDR;
                        buff1.data[0] = 0x1;
                        buff1.data[1] = buff.data[1];
                        buff1.data[2] = read_EEPROM(buff.data[1]);
                        buff1.data[3] = read_EEPROM(buff.data[1])>>8;
                        buff1.len = 4;
                        cc_LoadTx(buff1);
                        break;
                    case 201:
                        write_EEPROM(buff.data[1],buff.data[2]<<8|buff.data[3]);
                        break;
                    case 202:
                        restore_EEData();
                        break;
                    case 254:
                        RESET();
                        break;
                    case 255:
                        received_cnt++;
                        buff1.RTR = 0;
                        buff1.node_addr = (1<<10)|ADDR;
                        buff1.data[0]=0xff;
                        buff1.data[1]=buff1.node_addr;
                        buff1.data[2]=received_cnt & 0xff;
                        buff1.data[3]=received_cnt>>8;
                        buff1.data[4]=received_cnt>>16;
                        buff1.data[5]=received_cnt>>24;
                        buff1.len = 6;
                        cc_LoadTx(buff1);
                        break;
                    default:
                        DeviceSpecificCommand(buff);
                }
            } else
                DeviceSpecificCommand(buff);
            buffer = get_buffer_ready();
        }
    }
}
void SetCommandCallBack(void* CommandHandler){
    DeviceSpecificCommand = CommandHandler;
}

void default_CallBack(){
}