/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef BAREMETAL_H
#define	BAREMETAL_H

#include <xc.h> // include processor files - each processor file is guarded.  
#include "EEPROM.h"
#include "mcp2515.h"
#include "statusLED.h"
#include "AiScheduler.h"

#define LED_PERIOD_NORMAL 150
#define LED_PERIOD_ERROR 75

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */
    
    uint8_t ID_LED_status;
    uint8_t ID_STATUS_REPORT;
    uint8_t ID_ERR_REPORT;
    uint8_t error_code = 0;
    CAN_Data *Report_data_ptr = 0;
    CAN_Data default_data;
    uint16_t reset_cause;
    uint16_t ADDR, ADDR2;

    void init_bareMetal(uint16_t addr1,uint16_t addr2);
    void RP_Reset();
    void err_handler();
    void Received_Command();
    void SetCommandCallBack(void* CommandHandler);
    void default_CallBack();
    void periodic_report(uint16_t period);
    void status_report();

    

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */

