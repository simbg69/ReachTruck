import serial
import time
import threading

class igauge():
    def __init__(self):
        self.com = serial.Serial(
         port = '/dev/serial0',
         baudrate = 19200,
         parity = serial.PARITY_EVEN,
         stopbits = serial.STOPBITS_TWO,
         timeout = 1)
        
        self.alive = False
        self.batt = 0
        self.operated_hour = 0
        self.seg7 = 0
        self.error_code = None
        self.com.close()
        self.com.open()
        rx_thread = threading.Thread(target=self.rx)
        rx_thread.daemon = True
        rx_thread.start()
        
        alive_thread = threading.Thread(target=self.check_alive)
        alive_thread.daemon = True
        alive_thread.start()
            
        
    def rx(self):
        inFrame = False
        command = []
        counter = 0
        received = False
        while True:
            while self.com.inWaiting():
                data = ord(self.com.read())
                if inFrame:
                    command.append(data)
                    counter = counter + 1
                    if (counter >= 10):
                        inFrame = False
                        received = True
                else:
                    if data == 0x73:
                        inFrame = True
                        counter = 0
                        command = []
                
                if received == True:
                    self.alive = True
                    received = False
                    multiplier = 1
                    self.seg7 = 0
                    
                    for i in range (6):
                        if command[5-i] != 0x10:
                            if multiplier == 1:
                                self.seg7 = command[5-i]
                            else:
                                self.seg7 += command[5-i] * (multiplier)
                            multiplier *=10
                    if command[7] & 0x40:
                        self.operated_hour = self.seg7
                        self.error_code = None
                    if command[7] & 0x2:
                        self.batt = command[6]*10
                    if command[7] & 0x80:
                        self.error_code = 0
                        counter = 0
                        for i in range(6):
                            if command[5-i] !=0x10:
                                self.error_code |= (command[5-i] << counter)
                                counter += 4
                time.sleep(0.01)
            time.sleep(0.2)
         
    def check_alive(self):
        while True:
            self.alive = False
            time.sleep(5)
