from Can_Network import can_network
from struct import *
import pickle
import time
import random
################################################################################
#      PARAMETER TUNING                                                        #
################################################################################
# 0  Report Period        | Periodic Report rate, in ms. For fork, normally at 
#                         | 300
# 1  GOTO_Refresh_Rate    | The period for GOTO position to do calculation. Longer
#                         | time will be less responsive. Too fast will also not 
#                         | working as the motion didn't catch up with the control
#                         | work best at 10
# 2  FindHole_Refresh_Rate| Not use at the moment
# 3  Gross Tune           | Delta of the ADC before the fine tune happen. Used by
#                         | GOTO Position. Value too high will make the fork enter
#                         | fine tune motion too early, too low will cause the goto
#                         | never settle. Default 1000
# 4  Fine Tune            | Used in GOTO Position. This value determine when the 
#                         | the GOTO Position Stop. Smaller the value, better fork
#                         | position accuracy. Too small will make the GOTO never
#                         | stop. Further, this value should always larger then 
#                         | the sensor noise. Default 400
# 5  Fork Move Time       | Used in Fine Tune loop. The move fork time is multiple
#                         | of the GOTO_Refresh_Rate. Default 10
# 6  Fork_Stop_Time       | Used in Fine Tune loop. The fork stop time is multiple 
#                         | of the GOTO_Refresh_Rate. This time is used to throttle
#                         | the fork position. Default 30
# 7  Confidence_cnt       | Used by GOTO_Position. Higher the confidence_cnt, the
#                         | stop distance will more accurate fall in the Fine Tune
#                         | window. Too high will cuase the process never ending,
#                         | too low the fork might be stop at wrong position. 
#                         | This counter is in factor of GOTO_Refresh_Rate, hence,
#                         | the higher this value, the longer the settling time.
#                         | Default 50
# 8  Ground_Offset        | Put this value slightly higher than the minimum ADC
#                         | of the fork height. This will make the fork completly
#                         | reaching the ground. Default self.min_adc + 10
# 9  Reach_thres_out      | ADC value for Fork Reach OUT
# 10 Reach_thres_in       | ADC value for Fork Reach IN
# 11 Reach_timeout
# 12 TiltUp Timeout       | Constant time for Tilt Up
# 13 TiltNormal_timeout   | Constant time for Tilt Normal
# 14 GOTO timeout         | Factor of GOTO_Refresh_Rate before the GOTO process stop
################################################################################
class fork(can_network):
  def __init__(self, can_controller):
      can_network.__init__(self,can_controller,3)
      self.sense_left = 0
      self.sense_right = 0
      self.moving = 0
      self.height_adc = 0
      self.reach_adc = 0
      self.initial_adc = 0
      self.target_adc = 0
      self.max_height = 2950
      self.min_height = 0
      self.current_height = 0.05
      self.min_adc = 2816
      self.max_adc = 65472
      self.executing_goto = 0
      self.error = 0
      self.time_start = time.time()
      self.success = 0
      self.config = ['Report Period','GOTO_Refresh_Rate','FindHole_Refresh_Rate','Gross_Tune','Fine_Tune', \
        'Fork_Move_Time','Fork_Stop_Time','Confidence_cnt','Ground_Offset','Reach_thres_out', \
        'Reach_thres_in','Reach_TimeOut','TiltUp_timeout','TiltNormal_timeout','GOTO_TimeOut',\
        'SENSOR_CHECK_TIMEOUT']
      self.config_data = [0] * len(self.config)
      self.CAN_obj = can_controller
      
  def up(self):
    self.tx([0])
  
  def down(self):
    self.tx([1])
  
  def stop(self):
    self.tx([2])
    self.executing_goto = 0
    self.error = 0

  def goto_pos(self, height):
    target = int(self.min_adc + (self.max_adc - self.min_adc)*(height/(self.max_height - self.min_height)))
    self.goto(target)

  def goto(self,val):
    if self.executing_goto ==0 :
        self.executing_goto = 1
        self.error = 0
        self.success = 0
        if (val < self.min_adc):
            val = self.min_adc
        if (val > self.max_adc):
            val = self.max_adc
        self.initial_adc = self.height_adc
        tmp_val = [3] + list(pack('>H',val))
        self.tx(tmp_val)
        self.time_start = time.time()
        self.initial_adc = self.height_adc

  def reach_extend(self):
    self.tx([5])
  def reach_retract(self):
    self.tx([6])
  def reach_stop(self):
    self.tx([7])
  def reach_auto(self,action):
    self.tx([8,action])
  def fork_tilt(self):
    self.tx([9])
  def fork_Normal(self):
    self.tx([10])
  
  def rx_action(self,data):
    if data[0] == 1:
        tmp = unpack('h',bytearray(data[2:4]))[0]
        self.config_data[data[1]] = tmp
    elif (data[0] & 0xEF) == 2:
        self.sense_left = data[1] & 0x1
        self.sense_right = (data[1] & 0x2) >> 1
        self.moving = (data[1] & 0x4)>>2
        self.success = (data[1] & 0x8) >> 3
        self.executing = (data[1] & 0x10) >> 4
        self.height_adc = unpack('H',bytearray(data[2:4]))[0]
        self.target_adc = unpack('H',bytearray(data[4:6]))[0]
        self.reach_adc = unpack('H',bytearray(data[6:8]))[0]
        self.current_height = ((self.height_adc-self.min_adc) / (self.max_adc - self.min_adc))*(self.max_height)
        if (self.executing_goto == 1):
            if ((time.time() - self.time_start) > 10.0):
                if abs(self.initial_adc - self.height_adc) < 100 and self.success == 0 and abs(self.height_adc - self.target_adc)>5:
                    self.error = 1
                    self.executing_goto = 0
                elif self.success == 1:
                    self.executing_goto = 0
        
  def test(self,iteration):
    self.CAN_obj.logger.log("<<<<                FORK HEIGHT CONSISTENT TEST               >>>>",2)
    self.CAN_obj.logger.log("+----------+----------+----------+----------+----------+----------+",2)
    self.CAN_obj.logger.log("|  Round   |  Target  |  Actual  |   Delta  |  Begin   |   Time   |",2)
    self.CAN_obj.logger.log("|          |   (mm)   |   (mm)   |   (mm)   | Height mm|  second  |",2)
    self.CAN_obj.logger.log("+----------+----------+----------+----------+----------+----------+",2)
    for i in range (iteration):
      init_height = self.current_height
      init_time = time.time()
      target_height = random.randrange(self.min_height,self.max_height,10)
      self.goto_pos(target_height)
      while(self.executing_goto):
        pass
      self.CAN_obj.logger.log("|%-10i|%-10i|%-10i|%-10i|%-10i|%-10i|"%(i,target_height,self.current_height, \
          target_height-self.current_height,init_height,time.time()-init_time),2)
    self.CAN_obj.logger.log("+----------+----------+----------+----------+----------+----------+",2)
