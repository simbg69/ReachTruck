from Can_Network import can_network
from struct import *

class omronLaser(can_network):
    def __init__(self,can_controller):
        can_network.__init__(self,can_controller,6)
        self.check_alive_period = 2
        self.current_zone = 0
        self.rear_sensor_en_status = 0
        self.config =["Report Rate","SpeedThresSlow","SpeedThresMid","SpeedThresHigh"]
        self.config_data = [0] * len(self.config)
        self.refresh_config()
        self.sonar_raw_data=[0]*4 
    def rx_action(self,data):
        if data[0] == 1:
            tmp = unpack('h',bytearray(data[2:4]))[0]
            self.config_data[data[1]] = tmp
        elif (data[0] & 0xEF) == 2:
            self.sonar_raw_data = data[1:5]
            self.current_zone = data[6]
            self.rear_sensor_en_status = data[7]
    def set_zone(self,zone):
        self.tx([0,zone])
    def set_zone_auto(self,speed):
      rpm = int(speed*0.16);
      if (rpm != 0):
        self.tx([4] + list(pack('>h',rpm)))
      else:
        self.tx([1])

    def rear_sensor_power(self,en):
        self.tx([5,en])
