from Can_Network import can_network
from struct import *

class hokuyo(can_network):
    def __init__(self,can_controller):
        can_network.__init__(self,can_controller,5)
        self.received = []
        self.pallet_size = 0
        self.check_alive_period = 2
    
    def rx_action(self,data):
        self.received = data
        self.pallet_size = unpack('>H',bytearray(data[1:3]))[0]