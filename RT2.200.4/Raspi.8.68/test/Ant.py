import time
import threading
import operator
import re
from struct import *

class ant_lite:
    CAN_FC={
        'NMT':0,
        'TIME':0x20,
        'PDO1_TX':0x30,
        'PDO1_RX':0x40,
        'PDO2_TX':0x50,
        'PDO2_RX':0x60,
        'PDO3_TX':0x70,
        'PDO3_RX':0x80,
        'PDO4_TX':0x90,
        'PDO4_RX':0xA0,
        'SDO_TX':0xB0,
        'SDO_RX':0xC0,
        'NMT_ERR':0xD0,
        'HEARTBEAT':0xE0
    }
    CAN_NMT_CS={
        'reset_node':129,
        'reset_comm':130,
        'enter_pre_oper':128,
        'stop':2,
        'start':1,
    }
    CAN_NMT_STATE={
        'Stopped':4,
        'Operational':5,
        'Pre_operational':127
    }
    
    def __init__(self,can_controller,wheel,steer,fork,batt,omron,logger=None):
        self.can_obj = can_controller
        self.logger = logger
        if self.logger == None:
            self.logger = no_logger
        self.node_addr = 2
        self.target_speed = 0
        self.target_angle = 0
        self.move_fork_exec = False
        self.ANT_action_fail = 0
        self.ANT_action_success = 0
        self.ANT_OK = 0
        self.EN_steering = 0
        self.EN_wheel = 0
        self.move_fork_height = 0
        self.digital_output = 0
        self.x_coordinate = 0
        self.y_coordinate = 0
        self.orientation = 0
        self.actual_speed = 0
        self.actual_angle = 0
        self.move_fork_failure = 0
        self.move_fork_success = 0
        self.activate_ANT = 0
        self.ANT_mode = 0
        self.steering_ready = 0
        self.wheel_ready = 0
        self.digital_input = 0
        self.tx_actual_val = True
        self.error = []
        self.wheel = wheel
        self.steer = steer
        self.fork = fork
        self.batt=batt
        self.omron = omron
        self.battery_level = batt.batt
        self.battery_level = 0
        self.timeout = 0
        self.last_update = time.time()
        self.executing_fork = 0
        self.mission_data = [0]*8

        self.can_obj.command.update({self.CAN_FC['NMT']<<7:self.rx_nmt})
        self.can_obj.command.update({self.CAN_FC['SDO_RX']<<3|self.node_addr:self.rx_sdo_rx})
        self.can_obj.command.update({self.CAN_FC['PDO1_RX']<<3|self.node_addr:self.rx_target_val})
        self.can_obj.command.update({self.CAN_FC['PDO2_RX']<<3|self.node_addr:self.rx_position})
        self.can_obj.command.update({self.CAN_FC['PDO3_RX']<<3|self.node_addr:self.rx_ant_stat})
        self.can_obj.command.update({self.CAN_FC['PDO4_RX']<<3|self.node_addr:self.rx_unknown})
        self.state = self.CAN_NMT_STATE['Pre_operational']
        self.hb_duration = 0.3
        self.toogle = True
        self.heartBeat_producer = threading.Thread(target = self.hb_produce)
        self.heartBeat_producer.daemon = True
        self.heartBeat_producer.start()
        self.tx_actual_val_t = threading.Thread(target=self.tx_actual)
        self.tx_actual_val_t.daemon = True
        self.tx_actual_val_t.start()
        #simbg 20180212: Confirmed with BlueBotics this is not required by ANT
        #self.tx_actual_val_t2 = threading.Thread(target=self.tx_actual2)
        #self.tx_actual_val_t2.daemon = True
        #self.tx_actual_val_t2.start()
        #simbg 20180212
        self.last_horn = time.time()
        self.horn_timeout = 5.0
        #self.height_debaunce_int = 0
        #self.autoprong_debaunce()
    
    def show_status(self):
        tmp = ''
        my_var = vars(self)
        sorted_var = sorted(my_var.items(),key=operator.itemgetter(0))
        for i,j in sorted_var:
            tmp1 = re.match("[<_]",str(j))
            if  tmp1 == None:
                tmp += "%s\t\t%s\n"%(i,j)
        return tmp

    #def autoprong_debaunce(self):
        #if self.height_debaunce_int > 0 :
            #self.height_debaunce_int -= 1
            
        #threading.Timer(1, self.autoprong_debaunce).start()
        
        
    def tx(self,addr,data):
        self.can_obj.tx(addr,data)
        
    def rx_nmt(self,data):
        if data[0] == self.CAN_NMT_CS['reset_node']:
            _addr = self.CAN_FC['HEARTBEAT'] << 3 | self.node_addr
            self.state = self.CAN_NMT_STATE['Pre_operational']
            self.tx(_addr,[self.state])
        elif data[0] == self.CAN_NMT_CS['start']:
            _addr = self.CAN_FC['HEARTBEAT'] << 3 | self.node_addr
            self.state = self.CAN_NMT_STATE['Operational']
            self.tx(_addr,[self.state])
            self.tx_actual_val = True
    
    def rx_sdo_rx(self,data):
        if data[2]<<8 | data[1] == 0x1017:
            _addr = self.CAN_FC['SDO_TX'] << 3 | self.node_addr
            _data = [0] * 8
            _data[1:] = data[1:]
            _data[0] = 0x60
            self.hb_duration = (data[5]<<8|data[4]) *0.9/1000
            self.tx(_addr,_data)
    
    def rx_target_val(self,data):
        self.target_speed = (unpack('<h',bytearray(data[0:2])))[0]
        self.target_angle = (unpack('<h',bytearray(data[2:4])))[0]
        #print("target speed: " + str(self.target_speed) + ", " + str(self.target_angle))
        self.logger.log("rx target speed from ANT %s"%(str(self.target_speed)),0)
        self.move_fork_exec = (data[4] & 0x80) >> 7
        self.ANT_action_fail = (data[4] & 0x10) >> 4
        self.ANT_action_success = (data[4] & 0x08)>>3
        self.ANT_OK = (data[4] & 0x4)>>2
        self.EN_steering = (data[4] & 0x2) >>1
        self.EN_wheel = (data[4] & 1)
        self.digital_output = data[5]
        self.move_fork_height = (unpack('<h',bytearray(data[6:8])))[0]
        if self.ANT_mode == 0 and self.activate_ANT == 1:
            wheel_speed = self.target_speed /4
            self.wheel.move_pid(wheel_speed)
            self.steer.set_angle(self.target_angle/100)
            
            if (self.move_fork_exec == 1):
                if self.executing_fork == 0:
                    #if self.height_debaunce_int > 0:
                        #if self.last_executeheight == self.move_fork_height:
                             #return

                    print("goto position: " + str(self.move_fork_height))
                    self.last_executeheight = self.move_fork_height
                    #self.height_debaunce_int = 20
                    if self.move_fork_height == 32767:
                        #self.fork.find_hole()
                        self.fork.reach_auto(1)
                    elif self.move_fork_height == 32766:
                        self.fork.reach_auto(0)
                    elif self.move_fork_height >= 0:
                        self.fork.goto_pos(self.move_fork_height)
                    self.executing_fork = 1
            else:
                #self.fork.error = 0
                #self.fork.success = 0
                if self.executing_fork or self.move_fork_success: # don't auto-clear error, wait for human intervention
                    self.fork.stop()
                    self.fork.ifm.seek_abort()
                self.executing_fork = 0
            if self.digital_output & 0x20 == 0x20:
                self.ignore_safety()
            else:
                 self.resume_safety()
    
    def hb_produce(self):
        while True:
            _addr = (self.CAN_FC['HEARTBEAT'] << 3) | self.node_addr
            #_data = [(self.toogle) << 7 | self.state]
            _data = [self.state]
            self.tx(_addr,_data)
            self.toogle ^= 1
            time.sleep(self.hb_duration)
    
    def tx_actual(self):
        while True:
            if self.tx_actual_val:
                _data = [0]*8
                self.battery_level = self.batt.batt
                move_fork_success = self.move_fork_success
                if self.fork.ifm.running():
                    self.move_fork_success = self.fork.ifm.success
                else:
                    self.move_fork_success = self.fork.success
                move_fork_failure = self.move_fork_failure
                self.move_fork_failure = self.fork.error | self.fork.ifm.error
                self.wheel_ready = self.wheel.alive
                self.steering_ready = self.steer.alive
                self.digital_input = self.fork.sense_right << 1|self.fork.sense_left
                # Multipler is 6.25 by default. Since ANT expect 5000 rpm, try to put at 25
                self.actual_speed = int(self.wheel.rpm*25)
                self.actual_angle = int(self.steer.current_angle * 100)
                _data[1] = (self.actual_speed & 0xFF00) >>8
                _data[0] = (self.actual_speed & 0x00FF)
                _data[3] = (self.actual_angle & 0xFF00) >> 8
                _data[2] = (self.actual_angle & 0x00FF)
                _data[4] = self.move_fork_failure << 7 | self.move_fork_success << 6 | self.activate_ANT <<3 | self.ANT_mode << 2 | self.steering_ready << 1 | self.wheel_ready
                _data[5] = self.digital_input
                _data[6] = self.battery_level
                _data[7] = 0
                self.tx(0x182,_data)
                if (time.time() - self.last_update > 0.020):
                    self.timeout += 1
                    self.logger.log("ANT CAN TX Miss %d times"%self.timeout,1)
                else:
                    if ( move_fork_failure != self.move_fork_failure or
                      move_fork_success != self.move_fork_success ):
                      print(
                        "move_fork_failure=", _data[4]>>7 and 1,
                        "move_fork_success=", _data[4]>>6 and 1) #, 
                        #"activate_ANT=", _data[4]>>3 and 1, 
                        #"ANT_mode=", _data[4]>>2 and 1, 
                        #"steering_ready=", _data[4]>>1 and 1,
                        #"wheel_ready=", _data[4] and 1)
            self.last_update = time.time()
            time.sleep(0.015)
    
    def tx_actual2(self):
        while True:
            if self.tx_actual_val:
                self.tx(0x282,self.mission_data)
            time.sleep(0.3)
            
    def rx_unknown(self,data):
        self.status = []
        if data[0] & 0x2:
            self.status.append('iMovingForward')
        if data[0] & 0x4:
            self.status.append('iMovingBackward')
        if data[0] & 0x8:
            self.status.append('iTurningLeft')
        if data[0] & 0x10:
            self.status.append('iTurningRight')
        if data[0] & 0x20:
            self.status.append('iTurningOnTheSpot')
        if data[0] & 0x40:
            self.status.append('iPathStraight')
        if data[0] & 0x80:
            self.status.append('iPathLeft')
        if data[1] & 0x1:
            self.status.append('iPathRight')
            
    def rx_position(self,data):
        self.x_coordinate = (unpack('<i',bytearray(data[0:3]+([0] if data[2] <128 else [0xff]))))[0]/1000
        self.y_coordinate = (unpack('<i',bytearray(data[3:6]+([0] if data[5] <128 else [0xff]))))[0]/1000
        self.orientation = (unpack('<h',bytearray(data[6:8])))[0]
    def rx_ant_stat(self,data):
        self.error = []
        if data[0] & 0x2 :
            self.error.append('CDiagnosticsInProgress')
        if data[0] & 0x4 :
            self.error.append('CNoRangeDataAvailable')
        if data[0] & 0x10 :
            self.error.append('CTaskNotRunning')
        if data[0] & 0x20:
            self.error.append('cMonitoringFailed')
        if data[0] & 0x40:
            self.error.append('cSteeringCalibrationFailed')
        if data[1] & 0x04:
            self.error.append('cDiagnosticsRequired')
        if data[2] & 0x2:
            self.error.append('eLost')
        if data[2] & 0x4:
            self.error.append('eEncoderSpeedTooHigh')
        if data[2] & 0x10:
            self.error.append('eTooFarFromPath')
        if data[2] & 0x20:
            self.error.append('eUndefinedShape')
        if data[3] & 0x2:
            self.error.append('eMotionMismatch')
        if data[4] & 0x2:
            self.error.append('wLost')
        if data[4] & 0x4:
            self.error.append('wPause')
        if data[4] & 0x8:
            self.error.append('wStoppedObstacle')
            if time.time() - self.last_horn > self.horn_timeout:
                self.steer.ring_horn()
                self.last_horn = time.time()
        if data[5] & 0x10:
            self.error.append('wTooFarFromPath')
        if data[5] & 0x20:
            self.error.append('wLeftorTractionNotReady')
        if data[5] & 0x40:
            self.error.append('wSteeringNotReady')
        if data[6] & 0x2:
            self.error.append('nMoving')
        if data[6] & 0x4:
            self.error.append('nMissionRunning')
        if data[6] & 0x8:
            self.error.append('nLimitedObstacle')
    def ignore_safety(self):
        self.wheel.ignore_safety()
    def resume_safety(self):
        self.wheel.resume_safety()
        
class no_logger():
    def __init__(self):
        pass
    def log(text,level = 0):
        print (text)
