/**

**/
#include "mcc_generated_files/mcc.h"
#include "bareMetal.h"
#include "mcp2515.h"

#define MY_ADDR 0x6
#define MY_ADDR2 0x1

void set_zone(uint8_t field);
void set_io(uint8_t io);
void set_zone_auto(int speed);
void check_rear_sensor();
void device_specific_command(CAN_Data buff);

uint8_t *sonar_data;
CAN_Data Report_Data;
uint8_t *current_zone = 0;
uint8_t *rear_sensor;
uint8_t ignore_safety = 0;//simbg 20180115

void putStr(char *txStr)
{
    while (*txStr != 0)
    {
        putch(*txStr);
        txStr++;
    }
}

void uartPrintVersion()
{
    char version[16];
    sprintf(version, "%s", "Omron v0.4.0\n\r");
    putStr(version);
}

void main(void)
{
    SYSTEM_Initialize();
    init_bareMetal(MY_ADDR,MY_ADDR2);
    Report_data_ptr = &Report_Data;
    Report_Data.node_addr = MY_ADDR;
    sonar_data = &Report_Data.data[1];
    current_zone = &Report_Data.data[6];
    rear_sensor = &Report_Data.data[7];
    
    Report_Data.len = 8;
    SetCommandCallBack(device_specific_command);
    set_zone(5);
    register_thread(100,check_rear_sensor);
    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();

    SWDTEN = 1;
    
    //uartPrintVersion();
    
    while (1)
    {   
        thread_update();
        Received_Command();
        
        //uartPrintVersion();
    }
     
}
void device_specific_command(CAN_Data buff){
    if(buff.node_addr == MY_ADDR2){
        switch(buff.data[0]){
            case 0:
                set_zone(1);
                break;
            case 1:
                set_zone(5);
                break;
            case 4:
                set_zone_auto((int)(buff.data[1]<<8|buff.data[2]));
                break;
            case 5:
                if (buff.data[1]==1)
                {
                    ignore_safety = 1;
                    //Standby1_SetHigh();
                }
                else
                {
                    ignore_safety = 0;
                    //Standby1_SetLow();
                }
        }
    } else if(buff.node_addr == MY_ADDR){
        switch(buff.data[0]){
            case 0:
                set_zone(buff.data[1]);
                break;
            case 5:
                if (buff.data[1]==1)
                {
                    ignore_safety = 1;                    
                    //Standby1_SetHigh();
                }
                else
                {
                    ignore_safety = 0;                    
                    //Standby1_SetLow();
                }
        }
    }
}

void set_zone(uint8_t zone){
    switch (zone){
        case 1:
            set_io(0b0000111);
            break;
        case 2:
            set_io(0b0001011);
            break;
        case 3:
            set_io(0b0001101);
            break;
        case 4:
            set_io(0b0001110);
            break;
        case 5:
            set_io(0b0010011);
            break;
        case 6:
            set_io(0b0010101);
            break;
        case 7:
            set_io(0b0000110);
            break;
        case 8:
            set_io(0b0011001);
            break;
        case 9:
            set_io(0b0011010);
            break;
        case 10:
            set_io(0b0011100);
            break;
        case 11:
            set_io(0b0100011);
            break;
        case 12:
            set_io(0b0100101);
            break;
        case 13:
            set_io(0b0100110);
            break;
        case 14:
            set_io(0b0101001);
            break;
        case 15:
            set_io(0b0101010);
            break;
        case 16:
            set_io(0b0101100);
            break;
        case 17:
            set_io(0b0110001);
            break;
        case 18:
            set_io(0b0110010);
            break;
        case 19:
            set_io(0b0110100);
            break;
        case 20:
            set_io(0b0111000);
            break;
        case 21:
            set_io(0b1000011);
            break;
        case 22:
            set_io(0b1000101);
            break;
        case 23:
            set_io(0b1000110);
            break;
        case 24:
            set_io(0b1001001);
            break;
        case 25:
            set_io(0b1001010);
            break;
        case 26:
            set_io(0b1001100);
            break;
        case 27:
            set_io(0b1010001);
            break;
        case 28:
            set_io(0b1010010);
            break;
        case 29:
            set_io(0b1010100);
            break;
        case 30:
            set_io(0b1011000);
            break;
        case 31:
            set_io(0b1100001);
            break;
        case 32:
            set_io(0b1100010);
            break;
        case 33:
            set_io(0b1100100);
            break;
        case 34:
            set_io(0b1101000);
            break;
        case 35:
            set_io(0b1110000);
            break;
        default:
            set_io(0b0010011);
            break;
    }
    *current_zone = zone;
}
void set_io(uint8_t field){
    SetZone0_LAT = field & 1;
    SetZone1_LAT = (field & 2) >> 1;
    SetZone2_LAT = (field & 4) >> 2;
    SetZone3_LAT = (field & 8) >> 3;
    SetZone4_LAT = (field & 16)>> 4;
    SetZone5_LAT = (field & 32)>> 5;
    SetZone6_LAT = (field & 64)>> 6;
}

void set_zone_auto(int speed){
    
    //simbg 20180115:  For TzeLin's new safety zone 6
    if (ignore_safety)
    {
        if (speed >= 0 && speed <= EEDATA[SPEED1])
            set_zone(5);
        else
            set_zone(6); //Zone 6 takes care of NON-Pallet area SAFETY while loading or unloading pallet.
    }
    else
    {
        if (speed<0)
            set_zone(4);
        else if (speed > EEDATA[SPEED3])
            set_zone(3);
        else if (speed > EEDATA[SPEED2])
            set_zone(2);
        else if (speed > EEDATA[SPEED1])
            set_zone(1);
        else //simbg 20180115:  0 <= speed <= EEDATA[SPEED1]
            set_zone(5);
    }
}

void check_rear_sensor(){
    *rear_sensor = !(Standby1_GetValue());
}
/**
 End of File
*/