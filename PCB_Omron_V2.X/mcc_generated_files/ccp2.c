/**
  CCP2 Generated Driver File

  @Company
    Microchip Technology Inc.

  @File Name
    ccp2.c

  @Summary
    This is the generated driver implementation file for the CCP2 driver using MPLAB(c) Code Configurator

  @Description
    This source file provides APIs for CCP2.
    Generation Information :
        Product Revision  :  MPLAB(c) Code Configurator - 4.15.1
        Device            :  PIC16F1716
        Driver Version    :  2.00
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.35
        MPLAB             :  MPLAB X 3.40
*/

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

/**
  Section: Included Files
*/

#include <xc.h>
#include "ccp2.h"
#include "ccp1.h"

/**
  Section: Capture Module APIs
*/

void CCP2_Initialize(void)
{
    // Set the CCP2 to the options selected in the User Interface

    // CCP2M Falling edge; DC2B 0; 
    CCP2CON = 0x04;    

    // CCPR2L 0; 
    CCPR2L = 0x00;    

    // CCPR2H 0; 
    CCPR2H = 0x00;    

    // Clear the CCP2 interrupt flag
    PIR2bits.CCP2IF = 0;

    // Enable the CCP2 interrupt
    PIE2bits.CCP2IE = 1;
}

void CCP2_CaptureISR(void)
{
    CCP_PERIOD_REG_T module;

    // Clear the CCP2 interrupt flag
    PIR2bits.CCP2IF = 0;
    
    // Copy captured value.
    module.ccpr2l = CCPR2L;
    module.ccpr2h = CCPR2H;
    
    // Return 16bit captured value
    CCP2_CallBack(module.ccpr2_16Bit);
}

void CCP2_CallBack(uint16_t capturedValue)
{
    static char temp_sonar_data[5];
    // Add your code here
    static int8_t ccp_counter = -1;
    unsigned int period = capturedValue - CCP1_CaptureRead();
    uint8_t byte_count;
    bool data_bit = 0;
    if (period > 14000){
        ccp_counter = -2;
        for (int i = 0; i < 5; i ++){
            sonar_data[i] = 0;
        }
    }else{
        ccp_counter++;
    }
    if (period < 1000){
        data_bit = 1;
    }
    
    if (ccp_counter >=0){
        byte_count = ccp_counter/8;
        temp_sonar_data[byte_count] |= data_bit <<(8-(ccp_counter-(8*byte_count)));
        sonar_data[byte_count] |= data_bit <<(8-(ccp_counter-(8*byte_count)));
    }
    if (ccp_counter == 31){
        for (byte_count = 0; byte_count < 5; byte_count++)
        {
            sonar_data[byte_count] = temp_sonar_data[byte_count];
        }
        sonar_data[4] = 0;
    }
    
}
/**
 End of File
*/