import threading
import time
import operator
import re
from struct import *

class can_network():
    def __init__(self, can_controller, ID):
        self.CAN_ID = ID
        self.can_obj = can_controller
        self.alive = 0
        self.heartbeat = 0
        self.check_alive_period = 0.5
        self.last_update = time.time()
        self.reset_cause = ""
        self.config = []
        self.config_data = []
        self.rx_count = 0
        self.error_code = 0
        
        self.can_obj.command.update({(1<<10)|self.CAN_ID:self.rx})
        self.lost_packet = 0
        alive_thread_id = threading.Thread(target=self.check_alive)
        alive_thread_id.daemon = True
        alive_thread_id.start()        
    
    def check_alive(self):
        while True:
            if time.time() - self.last_update > self.check_alive_period:
                self.alive = 0
            else:
                self.alive = 1
            time.sleep(3)

    def tx(self,data):
        self.can_obj.tx(self.CAN_ID,data)
    
    def rx(self,data):
      self.last_update = time.time()
      self.rx_count += 1
      if len(data)>0:
        heartbeat = (data[0] & 0x80) >> 7
        if (heartbeat == self.heartbeat):
          self.lost_packet += 1
        self.heartbeat = heartbeat
        if(data[0] == 254):
          self.interprete_reset(data)
        elif (data[0] == 0):
          self.error_code = data[1]
        elif (data[0] == 253):
          print("IO Data State is ",data[1])
        self.rx_action(data)
    
    def rx_action(self,data):
         raise NotImplementedError("Must override rx_action")
    
    def disable_WDT(self):
        self.tx([129])
    
    def refresh_config(self):
        for x in range(len(self.config)):
            self.tx([200,x])
            time.sleep(0.01)
    def list_config(self):
        self.refresh_config()
        for i,x in enumerate(self.config):
            print(i,x,self.config_data[i])
    
    def set_EEPROM(self,addr,data):
        self.tx([201,addr]+list(pack('>h',data)))
        self.tx([202])
    
    def interprete_reset(self,data):
        reset = (data[1]<<8|data[2])
        self.can_obj.logger.log("Device ID %d Resetted"%(self.CAN_ID),1)
        if (reset & 0b111111011) == 0b001010011:
            self.reset_cause = "Power-on Reset"
        elif (reset & 0b110110111) == 0b000110011:
            self.reset_cause = "Brown-out Reset"
        elif (reset & 0b001000010) == 0b0:
            self.reset_cause = "WDT Reset"
        elif (reset & 0b000010000) == 0:
            self.reset_cause = "Reset Instruction"
        elif (reset & 0b100000000) == 0b100000000:
            self.reset_cause = "Stack Overflow Reset"
        elif (reset & 0x010000000) == 0b010000000:
            self.reset_cause = "Stack Underflow Reset"
        else:
            self.reset_cause = "Undefine Reset Cause"
    
    def reset(self):
        self.tx([254])
    
    def debug_mode(self):
        self.tx([130,0,0])
        self.tx([129,0,0])
    
    def read_io(self):
      self.io_state = 0;
      self.tx([191]);
    
    def set_io(self,io,state):
      self.tx([190,io,state])
        
    def show_status(self):
        tmp = ''
        my_var = vars(self)
        sorted_var = sorted(my_var.items(),key=operator.itemgetter(0))
        for i,j in sorted_var:
            tmp1 = re.match("[<_]",str(j))
            if  tmp1 == None:
                tmp += str(i)
                tmp += "\t: " if len(str(i))>10 else "\t\t: "
                tmp +=  str(j) + "\n"
        return tmp
        
