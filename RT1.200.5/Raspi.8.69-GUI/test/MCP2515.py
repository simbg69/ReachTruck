#cython: language_level=3

import spidev
import RPi.GPIO as GPIO
import time
import threading
from MCP2515_INC import MCP2515_REG as REG, MCP2515_MODE as MODE , MCP2515_SPI_MODE as SPI_MODE

class mcp_2515:
  nInstance = 0
  
  def __init__(self,device=0, int_pin=15, logger =None):
    self.rx_buffer = []
    GPIO.setmode(GPIO.BOARD)
    self.int_pin = int_pin
    self.logger = logger
    if self.logger == None:
      self.logger = no_logger()
    self.spi = spidev.SpiDev()
    self.spi.open(0,device)
    self.spi.mode = 0
    self.spi.max_speed_hz = 10000000
    self.spi.xfer2([SPI_MODE.reset])
    time.sleep(0.01)
    #change Mode to Config Mode
    self.mcp_set_mode(MODE.CONFIG)
    #disable CLKOUT, ONE SHOT MODE (bit 3)
    self.spi.xfer2([SPI_MODE.bit,REG.CANCTRL,0x0,0])
    self.TEC = 0
    self.REC = 0
    self.device = device
    #Configure 250KBPS. SJW-1, Crystal 20 Mhz
    self.mcp_write_reg(REG.CNF1,0xc1)
    self.mcp_write_reg(REG.CNF2,0xAC)
    self.mcp_write_reg(REG.CNF3,0x07)
    #Configure Rx Filter to NULL
    self.mcp_write_reg(REG.RXM0SIDH,0)
    self.mcp_write_reg(REG.RXM0SIDL,0)
    self.mcp_write_reg(REG.RXM0EID0,0)
    self.mcp_write_reg(REG.RXM0EID8,0)
    self.mcp_write_reg(REG.RXM1SIDH,0)
    self.mcp_write_reg(REG.RXM1SIDL,0)
    self.mcp_write_reg(REG.RXM1EID0,0)
    self.mcp_write_reg(REG.RXM1EID8,0)
    self.mcp_write_reg(REG.RXF0SIDH,0)
    self.mcp_write_reg(REG.RXF0SIDL,0)
    self.mcp_write_reg(REG.RXF1SIDH,0)
    self.mcp_write_reg(REG.RXF1SIDL,0)
    self.mcp_write_reg(REG.RXF2SIDH,0)
    self.mcp_write_reg(REG.RXF2SIDL,0)
    self.mcp_write_reg(REG.RXF3SIDH,0)
    self.mcp_write_reg(REG.RXF3SIDL,0)
    self.mcp_write_reg(REG.RXF4SIDH,0)
    self.mcp_write_reg(REG.RXF4SIDL,0)
    self.mcp_write_reg(REG.RXF5SIDH,0)
    self.mcp_write_reg(REG.RXF5SIDL,0)
    #Received standard msg, roll over to buff1 if full (0x24)
    self.mcp_write_reg(REG.RXB0CTRL,0x24)
    self.mcp_write_reg(REG.RXB1CTRL,0x20)
    #Disable all RxNBF Pin
    self.mcp_write_reg(REG.RXnBF,0)
    #Enable Interrupt for RX0, Rx1
    self.mcp_write_reg(REG.CANINTE,3)
    #Reset TEC, REC
    self.mcp_write_reg(REG.TEC,0)
    self.mcp_write_reg(REG.REC,0)
    #Clear All Interrupt
    self.mcp_write_reg(REG.CANINTF,0)
    
    #Setup GPIO Callback
    GPIO.setup(int_pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.add_event_detect(int_pin,GPIO.FALLING,callback=self.rx_cb)
    #return to Normal Operation
    self.mcp_set_mode(MODE.NORMAL)
    self.command = {}
    self.received_counter = 0
    
    check_err_t = threading.Thread(target=self.check_err)
    check_err_t.daemon = True
    check_err_t.start()
    
    rx_t = threading.Thread(target=self.rx_dispatch)
    rx_t.daemon = True
    rx_t.start()
        
    mcp_2515.nInstance += 1
    if (mcp_2515.nInstance == 1):
      blink_LED = threading.Thread(target=mcp_2515.LED)
      blink_LED.daemon = True
      blink_LED.start()

  def mcp_set_mode(self,mode):
    self.spi.xfer2([SPI_MODE.bit,REG.CANCTRL,0xE0,mode])
  
  def mcp_write_reg(self,addr,data):
    self.spi.xfer2([SPI_MODE.write,addr,data])
 
  def mcp_read_reg(self,addr):
    data = self.spi.xfer2([SPI_MODE.read,addr,0])
    return data[2]
  
  def rx_cb(self,pin):
    buffer_i = self.check_rx_ready()
    while buffer_i >0:
      if (buffer_i & 1 ):
        self.rx_buffer.append(self.spi.xfer2([SPI_MODE.read_rx0] + [0]*13))
      else:
        self.rx_buffer.append(self.spi.xfer2([SPI_MODE.read_rx1] + [0]*13))
      buffer_i = self.check_rx_ready()
  
  def rx_dispatch(self):
    while True:
      while len(self.rx_buffer)>0:
        tmp = self.rx_buffer.pop(0)
        addr =  ((tmp[1] << 3)&0x7FF)|((tmp[2]>>5)&0x7)
        data = tmp[6:tmp[5]+6]
        ptr = self.command.get(addr)
        self.logger.log("[CAN%d RX %.2f] addr- %d: %s"%(self.device,time.time(),addr,data),4)
        self.received_counter += 1
        if ptr != None:
          ptr(data)
      time.sleep(0.002)
          
  def check_rx_ready(self):
    buff = self.spi.xfer2([SPI_MODE.rx_status,0])
    return buff[1]>>6 & 3
  
  def tx(self,addr,data):
    engine = -1
    rts = -1
    txbuff = -1
    if self.check_tx_complete(0) == 0:
      engine = SPI_MODE.load_0
      rts = SPI_MODE.rts_0
      txbuff = 0
    elif self.check_tx_complete(1)== 0:
      engine = SPI_MODE.load_1
      rts = SPI_MODE.rts_1
      txbuff = 1
    elif self.check_tx_complete(2) == 0:
      engine = SPI_MODE.load_2
      rts = SPI_MODE.rts_2
      txbuff = 2
    else:
      self.logger.log("[CAN%d TX%d] addr- %d: TX ERROR, No Empty Buffer"%(self.device,txbuff,addr),0)
      return
    buff = [engine,(addr&0x7ff)>>3,(addr&7)<<5,0,0,len(data)] + data[:8]
    self.spi.xfer2(buff)
    self.spi.xfer2([rts])
    self.logger.log("[CAN%d TX%d] addr- %d: %s"%(self.device,txbuff,addr,data),3)
    
  def check_tx_complete(self,tx_buf):
    ret = self.spi.xfer2([SPI_MODE.read_status,0])
    return ret[1] & (1<<((tx_buf +1)*2))
  
  def read_all_reg(self):
    buff = [SPI_MODE.read] + [0]*0x81
    ret = self.spi.xfer2(buff)
    ret = ret[2:]
    for i in range (0x80):
      if (i % 8 == 0):
        print ("\n[%x]\t"%(i),end='')
      print ("%x\t"%(ret[i]),end='')
    print ("")
    
  @staticmethod
  def LED():
    LedState = 1
    LED = 12
    GPIO.setup(LED,GPIO.OUT)
    while True:
      GPIO.output(LED,LedState)
      LedState ^=1
      time.sleep(0.1)

  def check_err(self):
    while True:
        TEC = self.mcp_read_reg(REG.TEC)
        REC = self.mcp_read_reg(REG.REC)
        ERR = self.mcp_read_reg(REG.EFLG)
        if (TEC != self.TEC):
            self.TEC = TEC
            self.logger.log("CAN %d Tx Err -- %d"%(self.device,TEC),0)
        if (REC != self.REC):
            self.REC = REC
            self.logger.log("CAN %d Tx Err -- %d"%(self.device,REC),0)
        if ERR & 1 == 1:
            self.logger.log("CAN %d Tx|RX ERR > 96"%(self.device),0)
        if ERR & 2 == 2:
            self.logger.log("CAN %d RX WARN >96"%(self.device),0)
        if ERR & 4 == 4:
            self.logger.log("CAN %d TX WARN >96"%(self.device),0)
        if ERR & 8 == 8:
            self.logger.log("CAN %d RX ERR >128"%(self.device),0)
        if ERR & 16 == 16:
            self.logger.log("CAN %d TX ERR >128"%(self.device),0)
        if ERR & 32 == 32:
            self.logger.log("CAN %d TX OFF! ERR>255"%(self.device),0)
        if ERR & 64 == 64:
            self.logger.log("CAN %d RX0 OVERFLOW"%(self.device),0)
            self.mcp_write_reg(REG.EFLG,ERR & 0xBF)
        if ERR & 128 == 128:
            self.logger.log("CAN %d RX1 OVERFLOW"%(self.device),0)
            self.mcp_write_reg(REG.EFLG,ERR & 0x7F)
        time.sleep(3)

class no_logger():
  def __init__(self):
    self.log_level = 2
  def log(self,text,level = 0):
    if level < self.log_level:
      print (text)
