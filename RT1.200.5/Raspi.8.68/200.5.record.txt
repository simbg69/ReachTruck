20180108	Managed to complete multiple missions.

Issues found: 

1. Ant reports Traction and Steering not driving motor amplifiers and robot stopped moving.

It is still possible to issue "s.s.set_angle(x)" python commands in autonomous mode from raspberry pi, so firmware should still be alive.

2. Occasionally Rasperry Pi seems to send Success flag for fork action command too early to Ant ( e.g. 5002 - instruct fork to go to height 500mm and then retract ) before action has been fully executed, causing Ant to instruct AGV too early too to execute next action ( e.g. move backwards ).  
