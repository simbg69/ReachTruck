import time
from datetime import datetime
#import pytz

class stacker_logger():
  def __init__(self,textbox_obj=None, log_level=0):

    # get time in UTC
    #utc_dt = datetime.utcfromtimestamp(posix_timestamp).replace(tzinfo=pytz.utc)

    # convert it to tz
    #tz = pytz.timezone('Singapore')
    #dt = utc_dt.astimezone(tz)

    # print it
    #print(dt.strftime('%Y-%m-%d %H:%M:%S %Z%z'))
    
    #self.filename = "log_"+time.strftime("%Y_%m_%d_%H%M%S")+".log"
    self.filename = time.strftime("/home/pi/test/%Y_%m_%d_%H%M%S")+".log"
    #self.filename = dt.strftime("%Y_%m_%d_%H%M%S")+".log"
    self.textbox = None
    self._log_level = log_level
    self.verbose = False
    
    if textbox_obj != None:
      self.textbox = textbox_obj
  
  @property
  def log_level(self):
    return self._log_level
  
  @ log_level.setter
  def log_level(self,value):
    if value >3:
      value =3
    if value < 0:
      value = 0
    self._log_level = value

  def log(self,text,log_level = 0):
    if log_level <=self._log_level:
      #txt = "<%d> %.2f %s\n"%(log_level,time.time(),text)
      #txt = "<%d> %s %s\n"%(log_level, time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()), text)
      txt = "<%d> %s %s\n"%(log_level, datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f'), text)
      with open(self.filename, "a") as f:
        f.write(txt)
      if self.verbose == True:
        print(txt,end="")
    if self.textbox != None:
      try:
        with open(self.filename, "r+") as f:
          self.textbox.textlog = self.tail(f,8)
      except:
        pass
          
  def tail(self,f, lines=20):
    total_lines_wanted = lines
    BLOCK_SIZE = 1024
    f.seek(0,2)
    block_end_byte = f.tell()
    lines_to_go = total_lines_wanted
    block_number = -1
    blocks = [] # blocks of size BLOCK_SIZE, in reverse order starting
    # from the end of the file
    while lines_to_go > 0 and block_end_byte > 0:
      if (block_end_byte - BLOCK_SIZE > 0):
        # read the last block we haven't yet read
        f.seek(block_number*BLOCK_SIZE, 2)
        blocks.append(f.read(BLOCK_SIZE))
      else:
        # file too small, start from begining
        f.seek(0,0)
        # only read what was not read
        blocks.append(f.read(block_end_byte))
      lines_found = blocks[-1].count('\n')
      lines_to_go -= lines_found
      block_end_byte -= BLOCK_SIZE
      block_number -= 1
    all_read_text = ''.join(reversed(blocks))
    return '\n'.join(all_read_text.splitlines()[-       total_lines_wanted:])
