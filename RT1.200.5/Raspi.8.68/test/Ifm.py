import time
import threading
from queue import Queue
from struct import *
import xmlrpc.client

class ifm_seek_pocket:
    WAIT_STATE = {
        'WAIT_NONE':0,
        'WAIT_FORK':1,
        'WAIT_SEEK':2,
        'WAIT_ABORT':3,
    }
    SEEK_MSG = {
        'SEEK_START':0,
        'FORK_DONE':1,
        'SEEK_DONE':2,
        'SEEK_ABORT':3,
    }

    def __init__(self, fork, sensor_height_offset_mm = 500, sensor_height_tolerance_mm = 20, reach_action_to_trigger_pallet_seek = 9, tries_before_giveup = 3):

        self.sensor = xmlrpc.client.ServerProxy('http://192.168.8.69:5000')
        self.tries_before_giveup = tries_before_giveup

        #self.seek_pocket_success = 0
        #self.seek_pocket_error = 0
        #self.pocket_height = 0

        # constant variables
        self.sensor_height_offset_mm = sensor_height_offset_mm #482 #491 # TODO: Fine tune via [(return of seek_pocket())x(-1000)] with fork at exactly pocket position
        self.sensor_height_tolerance_mm = sensor_height_tolerance_mm #20 #30
        self.fork = fork
        self.reach_action = reach_action_to_trigger_pallet_seek #1

        # read-only variables
        self.height_target_mm = 666
        self.success = 0
        self.error = 0
        self.abort = False

         # private variables
        self.state = self.WAIT_STATE['WAIT_NONE']
        self.sleep_sec = 1
        self.q = Queue()
        self.t = threading.Thread(target = self.thread)
        self.t.daemon = True
        self.t.start()

    def running(self):
        if self.state == self.WAIT_STATE['WAIT_NONE']:
            return False
        return True

    def tx_msg(self, msg):
        self.q.put(msg)

    def seek_start(self, height):
        if not self.fork.executing_goto:
            if not self.running():
                self.height_target_mm = height
                self.tx_msg(self.SEEK_MSG['SEEK_START'])
                return True
        return False

    def fork_move(self): # private function
        while self.fork.success:
            if self.abort:
                break
            self.fork.stop() # to clear fork.success
            time.sleep(self.sleep_sec)
        if self.abort == False:
            self.state = self.WAIT_STATE['WAIT_FORK']
            self.fork.goto(self.height_target_mm, self.reach_action)
            #self.fork.goto(self.fork.height_mm2adc(self.height_target_mm), self.reach_action)

    def fork_done(self):
        if self.abort == False:
            if self.state == self.WAIT_STATE['WAIT_FORK']:
                self.tx_msg(self.SEEK_MSG['FORK_DONE'])
                return True
        return False

    def seek_done(self, height, success, error):
        if self.abort == False:
            if self.state == self.WAIT_STATE['WAIT_SEEK']:
                self.height_target_mm = height
                self.success = success
                self.error = error
                self.tx_msg(self.SEEK_MSG['SEEK_DONE'])
                return True
        return False

    def seek_abort(self):
        if self.running():
            self.abort = True
            self.tx_msg(self.SEEK_MSG['SEEK_ABORT'])
            return True
        return False

    def format_float(self, f):
        return "{0:.3f}".format(f)

    def seek_pocket(self):
        print("seek_pocket: START")
        #height_mm = self.fork.current_height
        #print("fork.current_height ", self.fork.current_height)
        success = 0
        error = 0
        pallet_height = 0
        delta_mm = 0.0
        #delta_m = self.sensor_height_offset_mm / 1000.0
        #print("delta_m ", delta_m)

        # TODO: Jacky's algo + check self.abort + set error
        # delta_m = from Jacky's algo
        # error = ?
        for i in range(self.tries_before_giveup):

            try:
                time.sleep(0.7) #(1.7)
                level_diff_from_sensor_to_pallet = self.sensor.find_closet_pallet_level()
                if level_diff_from_sensor_to_pallet is not None:
                    print("level_diff_from_sensor_to_pallet ", self.format_float(level_diff_from_sensor_to_pallet))
                    #delta_m += level_diff_from_sensor_to_pallet
                    error = 0
                    break
                else:
                    print("No pallet found, adjusting fork tilt to seek pallet pockets.")
                    self.fork.fork_Normal()
                    #simbg 20180417: Normalise fork downwards AND tilt up for Truck 1 
                    self.fork.fork_tilt()
                    time.sleep(0.7) 
                    #delta_m = 0
                    error = 1
                #print("delta_m ", delta_m)    
                #time.sleep(0.5)
            except Exception as ex:
                print(str(ex))
                error = 1

        #time.sleep(0.2)

        if error == 0: 
            #delta_mm = delta_m * 1000.0
            #delta_mm += self.sensor_height_offset_mm
            #height_mm += delta_mm
            level_diff_from_sensor_to_pallet *= 1000
            delta_mm = self.sensor_height_offset_mm + level_diff_from_sensor_to_pallet
            pallet_height = self.fork.current_height + delta_mm
            if abs(delta_mm) <= self.sensor_height_tolerance_mm:
                success = 1
                print("success: target height ", pallet_height, " and required adjustment ", delta_mm)
            else:
                print("Required adjustment ", delta_mm, " failed tolerance ", self.sensor_height_tolerance_mm)
            #success = 1
            
        #self.seek_done(height_mm, success, error)
        self.seek_done(pallet_height, success, error)
        print("seek_pocket: END")
        return delta_mm #delta_m

    def thread(self):
        while True:
            period_msec = self.fork.config_data[self.fork.config.index("Report Period")]
            if period_msec:
                self.sleep_sec = period_msec / 500.0 # 2x Fork FW report rate
            else:
                self.fork.list_config()
            # state = self.state
            msg = self.q.get()
            if msg == self.SEEK_MSG['SEEK_START']:
                if self.state == self.WAIT_STATE['WAIT_NONE']:
                    self.fork_move()
            elif msg == self.SEEK_MSG['FORK_DONE']:
                if self.state == self.WAIT_STATE['WAIT_FORK']:
                    self.state = self.WAIT_STATE['WAIT_SEEK']
                    self.seek_pocket()
                    #for i in range(self.tries_before_giveup):
                        #if self.seek_pocket() != 0.0:                           
                            #break
            elif msg == self.SEEK_MSG['SEEK_DONE']:
                if self.state == self.WAIT_STATE['WAIT_SEEK']:
                    if self.success or self.error or self.abort:
                        self.state = self.WAIT_STATE['WAIT_ABORT']
                    else:
                        self.fork_move()
            elif msg == self.SEEK_MSG['SEEK_ABORT']:
                self.state = self.WAIT_STATE['WAIT_NONE']
                self.success = 0
                self.error = 0
                self.abort = False
            self.q.task_done()
            # if state != self.state:
                # print("ifm state: " + str(state) + " -> " + str(self.state) + " | sec: " + str(self.sleep_sec))
            time.sleep(self.sleep_sec)
