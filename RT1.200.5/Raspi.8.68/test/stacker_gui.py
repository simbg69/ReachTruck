import sys
import UI
import PyQt5
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtCore import QTimer
import Pyro4
import time
import os

class MainWindow(QMainWindow, UI.Ui_MainWindow):
    
  def __init__(self):
    super(self.__class__, self).__init__()
    self.setupUi(self) # gets defined in the UI file
    
    self.timer = QTimer()
    self.timer.timeout.connect(self.tick)
    self.timer.start(1000)
    self.stacker = Pyro4.Proxy("PYRONAME:Aitech.stacker")
    self.pushButton_4.clicked.connect(self.comm_test)
    self.radioButton.clicked.connect(self.set_manual_mode)
    self.radioButton_2.clicked.connect(self.set_manual_mode)
    self.radioButton_3.clicked.connect(self.set_manual_mode)
    self.comboBox.currentIndexChanged.connect(self.set_log_level)
  
  def set_log_level(self):
    self.stacker.log_level = self.comboBox.currentIndex() + 1
    
  def set_manual_mode(self):
    if self.radioButton.isChecked(): #Joystick
      self.stacker.set_manual_mode(0)
    elif self.radioButton_2.isChecked(): #Manual Override
      self.stacker.set_manual_mode(1)
    else: #Autonomous
      self.stacker.set_manual_mode(2)
      print("In Automode")
  
  def comm_test(self):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)
    msg.setText("Communication Test. This will freeze entire system. Please wait...")
    retval = msg.exec_()
    attached = self.stacker.find_all_node()
    tx,rx = self.stacker.comm_test(100)
    msg.setText("Attached node addr {}".format(attached))
    retval = msg.exec_()
    res = ""
    for i in tx:
      res += "Node {} - TX {}/100 | RX {}/100\n".format(i,tx[i],rx[i])
    msg.setText("Result:: \n" + res)
    msg.exec_()
    
  def tick(self):
    curr_idx = self.tabWidget.currentIndex()
    if curr_idx == 0:
      self.progressBar.setValue(self.stacker.get_batt_level())
      self.checkBox_7.setCheckState(self.stacker.get_ant_alive())
      self.checkBox.setCheckState(self.stacker.get_wheel_alive())
      self.checkBox_2.setCheckState(self.stacker.get_steer_alive())
      self.checkBox_3.setCheckState(self.stacker.get_fork_alive())
      self.checkBox_4.setCheckState(self.stacker.get_omron_alive())
      self.checkBox_5.setCheckState(self.stacker.get_igauge_alive())
      self.checkBox_6.setCheckState(1)
      self.checkBox_8.setCheckState(self.stacker.get_hokuyo_alive())
      self.lcdNumber.display(self.stacker.get_wheel_speed())
      self.lcdNumber_2.display(self.stacker.get_steer_angle())
      self.lcdNumber_3.display(self.stacker.get_fork_height())
      self.lcdNumber_4.display(self.stacker.get_operated_hour())
      self.progressBar_2.setValue(self.stacker.get_omron_result())
      self.textBrowser.setText(self.stacker.get_log())
      self.lcdNumber_5.display(self.stacker.get_pallet_size())
    elif curr_idx == 1:
      module_idx = self.tabWidget_2.currentIndex()
      tmp = self.textBrowser_2.verticalScrollBar().value()
      if module_idx== 0:
        self.textBrowser_2.setText(self.stacker.get_ant_status())
      elif module_idx==1:
        self.textBrowser_2.setText(self.stacker.get_wheel_status())
      elif module_idx==2:
        self.textBrowser_2.setText(self.stacker.get_steer_status())
      elif module_idx==3:
        self.textBrowser_2.setText(self.stacker.get_fork_status())
      elif module_idx==4:
        self.textBrowser_2.setText(self.stacker.get_omron_status())
      elif module_idx==5:
        self.textBrowser_2.setText(self.stacker.get_hokuyo_status())
      self.textBrowser_2.verticalScrollBar().setValue(tmp)
      

if __name__ == "__main__":
  import sys
  app = QtWidgets.QApplication(sys.argv)
  default_gateway = "192.168.0.254"
  respond = -1
  while respond != 0:
    respond = os.system("ping -c 1 " + default_gateway)
    time.sleep(3)
  time.sleep(15)
  form = MainWindow()
  form.showFullScreen()
  #form.show()
  sys.exit(app.exec_())
