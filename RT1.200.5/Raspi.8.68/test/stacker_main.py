import Pyro4
import Pyro4.naming as NS
import MCP2515
import Wheel
import Ant
import Steer
import Fork
import iGauge
import logger
import GamePad
import Omron_Laser
import Hokuyo
import time
import struct
import multiprocessing
import os
import threading
import datetime

@Pyro4.behavior(instance_mode="single")
@Pyro4.expose
class Stacker(object):
  __instance = 0
  def __init__(self, log_level_val = 1):
    Stacker.__instance += 1
    self._instance = Stacker.__instance
    self.text = log_browser()
    self._log_level = log_level_val
    self.logger = logger.stacker_logger(self.text, self._log_level)
    self.a = MCP2515.mcp_2515(0,15,self.logger)
    self.b = MCP2515.mcp_2515(1,13,self.logger)
    self.w = Wheel.wheel(self.b)
    self.s = Steer.steer(self.b)
    self.f = Fork.fork(self.b)
    self.i = iGauge.igauge()
    self.o = Omron_Laser.omronLaser(self.b)
    #self.h = Hokuyo.hokuyo(self.b)
    self.ant = Ant.ant_lite(self.a,self.w,self.s,self.f,self.i,self.o,self.logger)
    self.gp = GamePad.gamepad()
    self.gp.button_Y_cb = self.js_fork_up
    self.gp.button_A_cb = self.js_fork_down
    self.gp.button_X_cb = self.js_reach_extend
    self.gp.button_B_cb = self.js_reach_retract
    self.gp.analog_Y_cb = self.js_move
    self.gp.analog_RX_cb = self.js_steer
    #self._log_level = 4
    self.attached_node = []
    self.initial_cnt = {}
    WDT_snoop_id = threading.Thread(target=self.snoop_WDT)
    WDT_snoop_id.daemon = True
    WDT_snoop_id.start()
    
  def snoop_WDT(self):
    while True:
        self.b.tx(0,[128])
        time.sleep(1)
  def js_reach_extend(self,state):
    if state == 1:
      self.f.reach_extend()
      #self.f.fork_tilt()
    else:
      self.f.reach_stop()
  def js_reach_retract(self,state):
    if state == 1:
      self.f.reach_retract()
      #self.f.fork_Normal()
    else:
      self.f.reach_stop()
  def js_fork_up(self,state):
    if state == 1:
      self.f.up()
      #self.f.fork_tilt()
    else:
      self.f.stop()
      #self.f.reach_stop()
  def js_fork_down(self,state):
    if state == 1:
      self.f.down()
      #self.f.fork_Normal()
    else:
      self.f.stop()
      #self.f.reach_stop()

  def js_move(self,value):
    val = -value >> 8
    if abs(val) > 2:
      speed = int(val/90*self.w.encoder_tpm)
      self.w.move_pid(speed)
    else:
      self.w.stop()
  def js_steer(self,value):
    val = -value >>8
    if (abs(val) <2):
      val = 0
    val= val/127*90
    self.s.set_angle(val)
  
  def get_ant_status(self):
    return self.ant.show_status()
  def get_mission_status(self):
    if self.ant.ANT_action_success == 1:
        return 1
    elif self.ant.ANT_action_fail == 1:
        return -1
    else:
        return 0
  @property
  def ant_digital_input(self):
    return self.ant.digital_input
  @ant_digital_input.setter
  def ant_digital_input(self,value):  
    self.ant.digital_input = value
  def get_ant_alive(self):
    return self.ant.state == 5
  def enable_ant(self,value):
    if value == 0:
      self.ant.activate_ANT = 0
      self.ant.ANT_mode = 1
    elif value == 1:
      self.ant.ANT_mode = 0 
      self.ant.activate_ANT = 1
      self.ant.state = 5
      self.ant.tx_actual_val = True
  
  def get_wheel_status(self):
    return self.w.show_status()
  def get_wheel_alive(self):
    return self.w.alive
  def get_wheel_speed(self):
    return self.w.vel
  @property
  def wheel_PID(self):
    return self.w.PID
  @wheel_PID.setter
  def wheel_PID(self,val):
    self.w.PID = val
    self.w.set_pid()

  def get_fork_status(self):
    return self.f.show_status()
  def get_fork_alive(self):
    return self.f.alive
  def get_fork_height(self):
    return self.f.current_height
    
  def get_steer_status(self):
    return self.s.show_status()
  def get_steer_alive(self):
    return self.s.alive
  def get_steer_angle(self):
    return self.s.current_angle
  def set_manual_mode(self,value):  
    if value == 0: #This is Joystick
        #self.w.set_joystick_mode()
        self.s.manual_mode(1)
        self.s.set_angle(0)
        self.enable_ant(0)
    elif value == 1: #Manual
        ##self.w.set_autonomous_mode()
        #self.w.set_joystick_mode()
        self.s.manual_mode(0)
        self.enable_ant(0)
    elif value == 2: #Auto
        #self.w.set_autonomous_mode()
        self.s.manual_mode(1)
        self.s.set_angle(0)
        self.enable_ant(1)
    
  @property
  def steer_PID(self):
    return self.s.PID
  @steer_PID.setter
  def steer_PID(self,val):
    self.s.PID = val
  def get_omron_status(self):
    return self.o.show_status()
  def get_omron_alive(self):
    return self.o.alive
  def get_omron_result(self):
    return self.o.current_zone 
    
  def get_hokuyo_status(self):
    return self.h.show_status()
  def get_hokuyo_alive(self):
    return self.h.alive
  def get_pallet_size(self):
    return self.h.pallet_size
  
  def get_batt_level(self):
    return self.i.batt
  def get_igauge_alive(self):
    return self.i.alive
  def get_operated_hour(self):
    return self.i.operated_hour
  
  def snap_node(self,node):
    self.ant.mission_data = [2] + [node&0xff]+[(node&0xFF00)>>8]+[0,0,0,0,0]
  def run_mission(self,mission,arg1=0,arg2=0):
    self.ant.mission_data = [1] + [mission&0xff]+[(mission&0xFF00)>>8]+[arg1&0xff]+[(arg1&0xFF00)>>8]+[arg2&0xff]+[(arg2&0xff00)>>8]+[0]
  def end_mission(self):
    self.ant.mission_data[0] = 0
  @property
  def instance(self):
    return self._instance

  @property
  def log_level(self):
    return self._log_level
    
  @log_level.setter
  def log_level(self,value):
    self._log_level = value

  def get_log(self):
    return self.text.textlog
  
  def find_all_node(self):
    backup = self.deepcopy(self.b.command)
    self.attached_node = []
    for i in range(0x7ff):
      self.b.command.update({i:self.tmp_command})
    self.b.tx(0,[255])
    time.sleep(1)
    self.b.command = self.deepcopy(backup)
    del backup
    return self.attached_node

  def tmp_command(self,data):
    if data[0] == 0xff:
      self.attached_node.append(data[1])
      cnt = struct.unpack('L',bytearray(data[2:6]))[0]
      self.initial_cnt.update({data[1]:cnt})
  def tmp_command2(self,data):
    if data[0] == 0xff:
      rx_counter = self.rx_result[data[1]] + 1
      self.rx_result.update({data[1]:rx_counter})
      cnt = struct.unpack('L',bytearray(data[2:6]))[0]
      self.tx_result.update({data[1]:cnt})

  def comm_test(self,iteration):
    #self.b.tx(0,[129]);
    self.b.tx(0,[130,0,0])
    self.find_all_node()
    
    self.tx_result = {}
    self.rx_result = {}
    backup = self.deepcopy(self.b.command)
    for i in self.attached_node:
      self.b.command.update({1<<10|i:self.tmp_command2})
      self.rx_result.update({i:0})
      self.tx_result.update({i:0})
    time.sleep(1)
    for i in range(iteration):
      for j in self.attached_node:
        while(self.b.check_rx_ready()>0):
          pass
        self.b.tx(j,[255])
        time.sleep(0.001)
      complete = (i*100/iteration)
      print(" In PROGRESS... %.2f%% of %i completed."%(complete,iteration),end="\r")
    time.sleep(2)
    self.b.command = self.deepcopy(backup)
    del backup
    for i in self.tx_result:
      tmp = self.tx_result[i]
      self.tx_result.update({i:self.tx_result[i] - self.initial_cnt[i]})
      self.initial_cnt[i] = tmp
    self.b.tx(0,[254])
    return [self.tx_result,self.rx_result]
  
  def stress_comm_test(self):
    self.logger.log("<<<<<<    RUNNING COMMUNICATION STRESS TEST    >>>>>>>",2)
    self.logger.log("+-------+------+----------+----------+---------------+",2)
    self.logger.log("| Round | Node |    TX    |     RX   |      Time     |",2)
    self.logger.log("+-------+------+----------+----------+---------------+",2)
    for i in range (30):
      start_time = time.time()
      self.comm_test(100000)
      end_time = time.time()
      duration = datetime.timedelta(0,end_time-start_time)
      self.logger.log("|%-7i|%6i|%10i|%10i|%15s|"%(i,self.attached_node[0],self.tx_result[self.attached_node[0]],\
          self.rx_result[self.attached_node[0]],duration),2)
      for j in range(1,len(self.attached_node)): 
        self.logger.log("|%-7s|%6i|%10i|%10i|%15s|"%("",self.attached_node[j],self.tx_result[self.attached_node[j]],\
          self.rx_result[self.attached_node[j]],""),2)
      self.logger.log("+-------+------+----------+----------+---------------+",2)
      time.sleep(5)
      
  def deepcopy(self,a):
    result = {}
    for i in a:
      result.update({i:a[i]})
    return result
  
  def fork_test(self):
    self.f.test(20)
 
  def wheel_test(self):
    self.w.test()
    
class log_browser():
    def __init__(self):
        self.textlog = ""

def setup_NS():
  NS.startNSloop("192.168.8.68",9090,enableBroadcast=True)
  
def main():
  time.sleep(5)
  my_obj = Stacker()
  
  default_gateway = "192.168.8.1"
  respond = -1
  while respond != 0:
    respond = os.system("ping -c 1 " + default_gateway)
    time.sleep(3)
  time.sleep(30)
  p =multiprocessing.Process(target=setup_NS)
  p.daemon = True
  p.start()
  
  Pyro4.config.REQUIRE_EXPOSE = False
  Pyro4.config.HOST="192.168.8.68"
  Pyro4.Daemon.serveSimple(
    {
      my_obj: "Aitech.stacker"
    },
    host = "192.168.8.68",
    ns = True)


if __name__ == "__main__":
  main()
