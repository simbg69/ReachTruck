from Can_Network import can_network
from struct import *
import time
import pickle
import random
import datetime

################################################################################
#      PARAMETER TUNING                                                        #
################################################################################

class wheel(can_network):
  zone_def = {
    0: 'Obstacle Clear',
    1: 'Front Blocked',
    2: 'Rear Blocked',
    3: 'Front Rear Blocked',
  }
  prev_enc = 0    
  def __init__(self, can_controller):
    can_network.__init__(self,can_controller,1)
    self.CAN_obj = can_controller
    self.encoder_val = 0
    self.current_DAC = 0
    self.bumper_state = 0
    self.zone = "Free"
    self.is_moving = 0
    self.max_DAC = 255
    self.min_DAC = -255
    self.rpm = 0
    self.max_vel = 0
    self.encoder_tpm = 1520
    self.PID = [15,2,0]
    self.vel = 0
    self.target_speed = 0
    self.reset_cause = None
    self.no_safety = 1
    self.ramp = 1
    self.report_rate = 0
    self.enc_timeout = 10
    self.config = ['Report Period','KP','KI','KD','PID_MAX','PID_MIN','RAMP','ENC TIMEOUT', 'DIRECTION', 'POLARITY']
    self.config_data = [0] * len(self.config)
    self.refresh_config()
      
  def move(self,dac_val):
    dac_val = int(dac_val)
    print("move %d, %s", dac_val, datetime.datetime.now())
    if dac_val > self.max_DAC:
      dac_val = self.max_DAC
    elif dac_val < (-self.max_DAC + 1):
      dac_val = -self.max_DAC +1
    test = [0] +list(pack('>h',dac_val))
    self.tx(test)
  
  def stop(self):
    self.tx([1])
  
  def set_encoder(self,val):
    val = [2] + list(pack('>l',val))     
    self.tx(val)
  
  def move_pid(self,rpm_input):
    rpm = int(rpm_input*0.16);
    #print("move_pid rpm %d, %s", rpm, datetime.datetime.now())
    self.CAN_obj.logger.log("move_pid rpm %d, rpm_input %f"%(rpm, rpm_input),1)
    if (rpm != 0):
      self.tx([4] + list(pack('>h',rpm)))      
    else:
      self.stop()
  
  def move_pid_ms(self,vel):
    rpm = int(vel * self.encoder_tpm /6.25)
    self.tx([4] + list(pack('>h',rpm)))

  def rx_action(self,data):
    if data[0] == 1:
      tmp = unpack('h',bytearray(data[2:4]))[0]
      self.config_data[data[1]] = tmp
      self.restore_EEPROM()
    elif (data[0] & 0xEF) == 2:
      temp = unpack('hhh',bytearray(data[2:8]))
      self.current_DAC = temp[2]
      self.rpm = temp[1]<<3;
      self.bumper_state = (data[1] >>1) & 0x3
      self.zone = self.zone_def[self.bumper_state]
      self.is_moving = data[1] & 1
      self.vel = (self.rpm*6.25)/self.encoder_tpm
      self.encoder_val = temp[0]
      if self.vel > self.max_vel:
        self.max_vel = self.vel
  
  def restore_EEPROM(self):
    self.report_rate = self.config_data[0]
    self.PID[0] = self.config_data[1]
    self.PID[1] = self.config_data[2]
    self.PID[2] = self.config_data[3]
    self.max_DAC = self.config_data[4]
    self.min_DAC = self.config_data[5]
    self.ramp = self.config_data[6]
    self.enc_timeout = self.config_data[7]
  
  def magic_update_EEPROM(self):
    self.set_EEPROM(0,self.report_rate)
    self.set_EEPROM(1,self.PID[0])
    self.set_EEPROM(2,self.PID[1])
    self.set_EEPROM(3,self.PID[2])
    self.set_EEPROM(4,self.max_DAC)
    self.set_EEPROM(5,self.min_DAC)
    self.set_EEPROM(6,self.ramp)
    self.set_EEPROM(7,self.enc_timeout)
      
  def ignore_safety(self):
    self.tx([5,1])
    self.no_safety = 1
  def resume_safety(self):
    self.tx([5,0])
    self.no_safety = 0
    
  def set_joystick_mode(self):
    #self.tx([7,1])
    self.tx([7,0])
    
  def set_autonomous_mode(self):
    self.tx([6,1])

  def test(self,iteration):
    self.CAN_obj.logger.log("<<<<             WHEEL ENCODER CONSISTENT TEST           >>>>",2)
    self.CAN_obj.logger.log("+-------+--------+--------+-------+-------+-------+---------+--------+",2)
    self.CAN_obj.logger.log("| Round | Target | Actual | Delta | Stop  | Speed |  Time   | Result |",2)
    self.CAN_obj.logger.log("|       |  (mm)  |  (mm)  |  (mm) | Dist. |  m/s  | second  |        |",2)
    self.CAN_obj.logger.log("+-------+--------+--------+-------+-------+-------+---------+--------+",2)
    for i in range (iteration):
      start_time = time.time()
      start_enc = self.encoder_val
      speed = 0.1
      distance = 1000
      distance_tick = distance*self.encoder_tpm/1000
      direction = 1 if i%2==0 else -1
      self.move_pid_ms(speed*direction)
      while direction*self.encoder_val < distance_tick * direction + start_enc:
        pass
      self.stop()
      stop_time = time.time()
      end_enc = self.encoder_val
      time.sleep(3)
      curr_enc = self.encoder_val
      result = "PASS"
      delta = abs(curr_enc - start_enc)*1000/self.encoder_tpm
      stop_dist = (curr_enc - end_enc)*1000/self.encoder_tpm
      if (delta-distance) > (distance_tick *0.1)*1000/self.encoder_tpm:
        result = "F Dist"
      elif (stop_time-start_time) > (speed * distance)*1.2:
        result = "F Time"
      self.CAN_obj.logger.log("|%7d|%8d|%8d|%7d|%7d|%7.2f|%9d|%8s|"%(i,distance,delta, \
         delta-distance,stop_dist,speed,stop_time - start_time,result),2)
    self.CAN_obj.logger.log("+-------+--------+--------+-------+-------+-------+---------+--------+",2)
class no_logger():
    def __init__(self):
        pass
    def log(text,level = 0):
        print (text)
