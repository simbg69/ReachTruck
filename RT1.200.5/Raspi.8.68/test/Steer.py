from Can_Network import can_network
from struct import *
import time
import pickle

################################################################################
#      PARAMETER TUNING                                                        #
################################################################################
# 0  report_rate      | Periodic Report rate, in ms. Default is 100
# 1  KP               | PID tuning. This can be factor by pid_factor.
# 2  KI               | PID tuning. This can be factor by pid_factor.
# 3  KD               | PID tuning. This can be factor by pid_factor.
# 4  max_DAC          | PID limit on the MAX output
# 5  min_DAC          | PID limit on the MIN output
# 6  pid_factor       | Factor the Kp,Ki and Kd value. This factor is the bit shift
#                     | after the multiplication with the error. 1 is divide by 2.
# 7  pid_out_factor   | This is a overall factor for the PID result.
# 8  horn_duration    | in ms. This is a constant time for the horn ring and 
#                     | stop ring. Default 50.
# 9  enc_threshold    | This value determine wheather the steering is stop moving.
#                     | In fact, this value is the noise level of the encoder.

class steer(can_network):
  def __init__(self, can_controller):
    can_network.__init__(self,can_controller,2)
    self.max_allow_DAC = 204
    self.min_allow_DAC = 50
    self.current_angle = 0
    self.current_encoder = 0
    self.current_DAC = 0
    self.max_angle = 90
    self.target_pos = 0
    self.moving = 0
    self.tick_per_deg = 12000/90
    self.PID = [50,2,0]
    self.report_rate = 200
    self.pid_factor = 4
    self.pid_out_factor = 4
    self.horn_duration = 50
    self.enc_threshold = 100
    self.config = ["REPORT_PERIOD","KP","KI","KD","PID_MAX","PID_MIN",\
        "PID_FACTOR","PID_OUT_FACTOR","HORN_DURATION","ENC_THRESHOLD","CENTER_DAC","PID_DURATION"]
    self.config_data = [0] * len(self.config)

  def set_dac(self,dac_val):
    dac_val = int(dac_val)
    if dac_val < self.min_allow_DAC:
        dac_val = self.min_allow_DAC
    elif dac_val > self.max_allow_DAC:
        dac_val = self.max_allow_DAC
    self.tx([0] + [dac_val])

  def set_angle(self,angle):
    self.target_pos = int(-angle*self.tick_per_deg)
    self.goto_pos(self.target_pos)

  def goto_pos(self, pos):
    self.tx([3] + list(pack('>h',pos)))

  def go_center(self):
    self.tx([2])
  
  def calibrate(self):
    self.tx([6])
    time.sleep(2)
    self.tx([100])

  def set_encoder(self,val):
    tmp_val = [1] + list(pack('>h',val))
    self.tx(tmp_val)

  def manual_mode(self,mode):
    self.tx([5,mode])
  
  def debug_PID(self,mode):
    self.tx(0x80,mode)
    
  def rx_action(self,data):
    if data[0] == 1:
      tmp = unpack('h',bytearray(data[2:4]))[0]
      self.config_data[data[1]] = tmp
      self.restore_EEPROM()
    elif (data[0] & 0xEF) == 2:
      self.current_encoder = unpack('h',bytearray(data[2:4]))[0]
      self.current_DAC = unpack('h',bytearray(data[4:6]))[0]
      self.current_angle = -self.current_encoder / self.tick_per_deg
      #beng: removing negative sign '-', likely previous Steer/Fork board firmware implemented related code in reverse polarity OR truck manufacturer wired related cabling on this truck the reverse way from other trucks. 
      #self.current_angle = self.current_encoder / self.tick_per_deg
      self.moving = data[1] & 1 ^ 1 
    elif (data[0] & 0xEF) == 0x20:
      tmp = unpack('hhh',bytearray(data[1:7]))
      self.debug_P = tmp[0]
      self.debug_I = tmp[1]
      self.debug_D = tmp[2]
    elif (data[0] * 0xEF) == 0x21:
      self.debug_v = unpack('h',bytearray(data[1:3]))
  
  def restore_EEPROM(self):
    self.report_rate = self.config_data[0]
    self.PID[0] = self.config_data[1]
    self.PID[1] = self.config_data[2]
    self.PID[2] = self.config_data[3]
    self.max_DAC = self.config_data[4]
    self.min_DAC = self.config_data[5]
    self.pid_factor = self.config_data[6]
    self.pid_out_factor = self.config_data[7]
    self.horn_duration =self.config_data[8]
    self.enc_threshold = self.config_data[9]
    
  def magic_update_EEPROM(self):
    self.set_EEPROM(0,self.report_rate)
    self.set_EEPROM(1,self.PID[0])
    self.set_EEPROM(2,self.PID[1])
    self.set_EEPROM(3,self.PID[2])
    self.set_EEPROM(4,self.max_DAC)
    self.set_EEPROM(5,self.min_DAC)
    self.set_EEPROM(6,self.pid_factor)
    self.set_EEPROM(7,self.pid_out_factor)
    self.set_EEPROM(8,self.horn_duration)
    self.set_EEPROM(9,self.enc_threshold)
    
  def find_center(self):
    self.tx([100])

  def ring_horn(self):
    self.tx([4,2])
