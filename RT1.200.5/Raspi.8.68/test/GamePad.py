import evdev as ev
import threading

class gamepad():
    def __init__(self):
        self.devices = [ev.InputDevice(fn) for fn in ev.list_devices()]
        self.button_A_cb = None
        self.button_B_cb = None
        self.button_X_cb = None
        self.button_Y_cb = None
        self.analog_X_cb = None
        self.analog_Y_cb = None
        self.analog_RX_cb = None
        self.analog_RY_cb = None
        try:
            self.gp = ev.InputDevice(self.scan_gamepad())
            readbutton_t = threading.Thread(target=self.read_button)
            readbutton_t.daemon=True
            readbutton_t.start()
        except:
            return None
    
    def scan_gamepad(self):
        device_fn = None
        for device in self.devices:
            if device.name == 'Logitech Gamepad F710' or device.name == 'Logitech Logitech Cordless RumblePad 2':
                device_fn = device.fn
        return device_fn
    def read_button(self):
        for event in self.gp.read_loop():
            if event.type == ev.ecodes.EV_ABS:
                if event.code == ev.ecodes.ABS_X:
                    if self.analog_X_cb != None:
                        self.analog_X_cb(event.value)
                elif event.code == ev.ecodes.ABS_Y:
                    if self.analog_Y_cb != None:
                        self.analog_Y_cb(event.value)
                elif event.code == ev.ecodes.ABS_RX:
                    if self.analog_RX_cb != None:
                        self.analog_RX_cb(event.value)
                elif event.code == ev.ecodes.ABS_RY:
                   if self.analog_RY_cb != None:
                        self.analog_RY_cb(event.value)
            if event.type == ev.ecodes.EV_KEY:
                keyevent = ev.categorize(event)
                if keyevent.keycode[0] == 'BTN_A':
                    if self.button_A_cb != None:
                        self.button_A_cb(keyevent.keystate)
                elif keyevent.keycode[0] == 'BTN_B':
                    if self.button_B_cb != None:
                        self.button_B_cb(keyevent.keystate)
                elif keyevent.keycode[1] == 'BTN_X':
                    if self.button_X_cb != None:
                        self.button_X_cb(keyevent.keystate)
                elif keyevent.keycode[1] == 'BTN_Y':
                   if self.button_Y_cb != None:
                        self.button_Y_cb(keyevent.keystate)
    
        