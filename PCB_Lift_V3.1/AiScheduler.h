/* 
 * File:   AiScheduler.h
 * Author: Jonathan
 *
 * Created on October 29, 2016, 12:04 PM
 */

#ifndef AISCHEDULER_H
#define	AISCHEDULER_H

#include <xc.h>
#include "mcc_generated_files/mcc.h"

#define MAX_THREAD 10

#ifdef	__cplusplus
extern "C" {
#endif

    unsigned int scheduler_ticks = 0;
    struct {
     bool start;
     bool timeup;
     unsigned int th_period;
     unsigned int next_occurance;
     void (*func_ptr)();
    }scheduler_thread[MAX_THREAD];
    
    uint8_t thread_cnt = 0;
    void scheduler_init();
    void timeUpdater();
    void thread_update();
    uint8_t register_thread(unsigned int period, void *f_ptr);
    void thread_pause(uint8_t thread);
    void thread_resume(uint8_t thread);
    void thread_reschedule(uint8_t thread,uint16_t time);
    
#ifdef	__cplusplus
}
#endif

#endif	/* AISCHEDULER_H */

