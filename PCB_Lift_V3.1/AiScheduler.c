#include "AiScheduler.h"

void scheduler_init(){
    TMR2_SetInterruptHandler(timeUpdater);
}

void timeUpdater(){
    scheduler_ticks++;
    for (int i=0; i<thread_cnt; i++){
        if(scheduler_ticks == scheduler_thread[i].next_occurance){
            scheduler_thread[i].timeup = scheduler_thread[i].start==1?1:0;
        }
    }    
}

uint8_t register_thread(unsigned int period, void *f_ptr){
    scheduler_thread[thread_cnt].timeup = 0;
    scheduler_thread[thread_cnt].th_period = period;
    scheduler_thread[thread_cnt].func_ptr = f_ptr;
    scheduler_thread[thread_cnt].next_occurance = scheduler_ticks + period;
    scheduler_thread[thread_cnt].start = true;
    if(thread_cnt < MAX_THREAD)
        thread_cnt++;
    return thread_cnt -1;
}

void thread_update(){
    for(int i=0; i <thread_cnt; i++){
        if (scheduler_thread[i].timeup){
            scheduler_thread[i].timeup = 0;
            scheduler_thread[i].func_ptr();
            scheduler_thread[i].next_occurance = scheduler_ticks + scheduler_thread[i].th_period;
        }
    }
}

void thread_pause(uint8_t thread){
    scheduler_thread[thread].start = 0;
}

void thread_resume(uint8_t thread){
    scheduler_thread[thread].timeup = 1;
    scheduler_thread[thread].start = 1;
}

void thread_reschedule(uint8_t thread,uint16_t time){
    scheduler_thread[thread].th_period = time;
    scheduler_thread[thread].timeup = 1;
    scheduler_thread[thread].start = 1;
}