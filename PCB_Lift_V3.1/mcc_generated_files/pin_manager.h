/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using MPLAB(c) Code Configurator

  @Description:
    This header file provides implementations for pin APIs for all pins selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB(c) Code Configurator - 4.15.1
        Device            :  PIC16F1716
        Version           :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.35
        MPLAB             :  MPLAB X 3.40

    Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.

    Microchip licenses to you the right to use, modify, copy and distribute
    Software only when embedded on a Microchip microcontroller or digital signal
    controller that is integrated into your product or third party product
    (pursuant to the sublicense terms in the accompanying license agreement).

    You should refer to the license agreement accompanying this Software for
    additional information regarding your rights and obligations.

    SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
    EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
    MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
    IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
    CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
    OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
    INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
    CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
    SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
    (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

*/


#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set Pallet_Sense aliases
#define Pallet_Sense_TRIS               TRISAbits.TRISA0
#define Pallet_Sense_LAT                LATAbits.LATA0
#define Pallet_Sense_PORT               PORTAbits.RA0
#define Pallet_Sense_WPU                WPUAbits.WPUA0
#define Pallet_Sense_OD                ODCONAbits.ODA0
#define Pallet_Sense_ANS                ANSELAbits.ANSA0
#define Pallet_Sense_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define Pallet_Sense_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define Pallet_Sense_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define Pallet_Sense_GetValue()           PORTAbits.RA0
#define Pallet_Sense_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define Pallet_Sense_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define Pallet_Sense_SetPullup()      do { WPUAbits.WPUA0 = 1; } while(0)
#define Pallet_Sense_ResetPullup()    do { WPUAbits.WPUA0 = 0; } while(0)
#define Pallet_Sense_SetPushPull()    do { ODCONAbits.ODA0 = 1; } while(0)
#define Pallet_Sense_SetOpenDrain()   do { ODCONAbits.ODA0 = 0; } while(0)
#define Pallet_Sense_SetAnalogMode()  do { ANSELAbits.ANSA0 = 1; } while(0)
#define Pallet_Sense_SetDigitalMode() do { ANSELAbits.ANSA0 = 0; } while(0)

// get/set ENC_B aliases
#define ENC_B_TRIS               TRISAbits.TRISA1
#define ENC_B_LAT                LATAbits.LATA1
#define ENC_B_PORT               PORTAbits.RA1
#define ENC_B_WPU                WPUAbits.WPUA1
#define ENC_B_OD                ODCONAbits.ODA1
#define ENC_B_ANS                ANSELAbits.ANSA1
#define ENC_B_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define ENC_B_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define ENC_B_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define ENC_B_GetValue()           PORTAbits.RA1
#define ENC_B_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define ENC_B_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define ENC_B_SetPullup()      do { WPUAbits.WPUA1 = 1; } while(0)
#define ENC_B_ResetPullup()    do { WPUAbits.WPUA1 = 0; } while(0)
#define ENC_B_SetPushPull()    do { ODCONAbits.ODA1 = 1; } while(0)
#define ENC_B_SetOpenDrain()   do { ODCONAbits.ODA1 = 0; } while(0)
#define ENC_B_SetAnalogMode()  do { ANSELAbits.ANSA1 = 1; } while(0)
#define ENC_B_SetDigitalMode() do { ANSELAbits.ANSA1 = 0; } while(0)

// get/set RA2 procedures
#define RA2_SetHigh()    do { LATAbits.LATA2 = 1; } while(0)
#define RA2_SetLow()   do { LATAbits.LATA2 = 0; } while(0)
#define RA2_Toggle()   do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define RA2_GetValue()         PORTAbits.RA2
#define RA2_SetDigitalInput()   do { TRISAbits.TRISA2 = 1; } while(0)
#define RA2_SetDigitalOutput()  do { TRISAbits.TRISA2 = 0; } while(0)
#define RA2_SetPullup()     do { WPUAbits.WPUA2 = 1; } while(0)
#define RA2_ResetPullup()   do { WPUAbits.WPUA2 = 0; } while(0)
#define RA2_SetAnalogMode() do { ANSELAbits.ANSA2 = 1; } while(0)
#define RA2_SetDigitalMode()do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set Lift_Down aliases
#define Lift_Down_TRIS               TRISAbits.TRISA3
#define Lift_Down_LAT                LATAbits.LATA3
#define Lift_Down_PORT               PORTAbits.RA3
#define Lift_Down_WPU                WPUAbits.WPUA3
#define Lift_Down_OD                ODCONAbits.ODA3
#define Lift_Down_ANS                ANSELAbits.ANSA3
#define Lift_Down_SetHigh()            do { LATAbits.LATA3 = 1; } while(0)
#define Lift_Down_SetLow()             do { LATAbits.LATA3 = 0; } while(0)
#define Lift_Down_Toggle()             do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define Lift_Down_GetValue()           PORTAbits.RA3
#define Lift_Down_SetDigitalInput()    do { TRISAbits.TRISA3 = 1; } while(0)
#define Lift_Down_SetDigitalOutput()   do { TRISAbits.TRISA3 = 0; } while(0)
#define Lift_Down_SetPullup()      do { WPUAbits.WPUA3 = 1; } while(0)
#define Lift_Down_ResetPullup()    do { WPUAbits.WPUA3 = 0; } while(0)
#define Lift_Down_SetPushPull()    do { ODCONAbits.ODA3 = 1; } while(0)
#define Lift_Down_SetOpenDrain()   do { ODCONAbits.ODA3 = 0; } while(0)
#define Lift_Down_SetAnalogMode()  do { ANSELAbits.ANSA3 = 1; } while(0)
#define Lift_Down_SetDigitalMode() do { ANSELAbits.ANSA3 = 0; } while(0)

// get/set Lift_Up aliases
#define Lift_Up_TRIS               TRISAbits.TRISA5
#define Lift_Up_LAT                LATAbits.LATA5
#define Lift_Up_PORT               PORTAbits.RA5
#define Lift_Up_WPU                WPUAbits.WPUA5
#define Lift_Up_OD                ODCONAbits.ODA5
#define Lift_Up_ANS                ANSELAbits.ANSA5
#define Lift_Up_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define Lift_Up_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define Lift_Up_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define Lift_Up_GetValue()           PORTAbits.RA5
#define Lift_Up_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define Lift_Up_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define Lift_Up_SetPullup()      do { WPUAbits.WPUA5 = 1; } while(0)
#define Lift_Up_ResetPullup()    do { WPUAbits.WPUA5 = 0; } while(0)
#define Lift_Up_SetPushPull()    do { ODCONAbits.ODA5 = 1; } while(0)
#define Lift_Up_SetOpenDrain()   do { ODCONAbits.ODA5 = 0; } while(0)
#define Lift_Up_SetAnalogMode()  do { ANSELAbits.ANSA5 = 1; } while(0)
#define Lift_Up_SetDigitalMode() do { ANSELAbits.ANSA5 = 0; } while(0)

// get/set Tip1 aliases
#define Tip1_TRIS               TRISAbits.TRISA6
#define Tip1_LAT                LATAbits.LATA6
#define Tip1_PORT               PORTAbits.RA6
#define Tip1_WPU                WPUAbits.WPUA6
#define Tip1_OD                ODCONAbits.ODA6
#define Tip1_SetHigh()            do { LATAbits.LATA6 = 1; } while(0)
#define Tip1_SetLow()             do { LATAbits.LATA6 = 0; } while(0)
#define Tip1_Toggle()             do { LATAbits.LATA6 = ~LATAbits.LATA6; } while(0)
#define Tip1_GetValue()           PORTAbits.RA6
#define Tip1_SetDigitalInput()    do { TRISAbits.TRISA6 = 1; } while(0)
#define Tip1_SetDigitalOutput()   do { TRISAbits.TRISA6 = 0; } while(0)
#define Tip1_SetPullup()      do { WPUAbits.WPUA6 = 1; } while(0)
#define Tip1_ResetPullup()    do { WPUAbits.WPUA6 = 0; } while(0)
#define Tip1_SetPushPull()    do { ODCONAbits.ODA6 = 1; } while(0)
#define Tip1_SetOpenDrain()   do { ODCONAbits.ODA6 = 0; } while(0)

// get/set Tip2 aliases
#define Tip2_TRIS               TRISAbits.TRISA7
#define Tip2_LAT                LATAbits.LATA7
#define Tip2_PORT               PORTAbits.RA7
#define Tip2_WPU                WPUAbits.WPUA7
#define Tip2_OD                ODCONAbits.ODA7
#define Tip2_SetHigh()            do { LATAbits.LATA7 = 1; } while(0)
#define Tip2_SetLow()             do { LATAbits.LATA7 = 0; } while(0)
#define Tip2_Toggle()             do { LATAbits.LATA7 = ~LATAbits.LATA7; } while(0)
#define Tip2_GetValue()           PORTAbits.RA7
#define Tip2_SetDigitalInput()    do { TRISAbits.TRISA7 = 1; } while(0)
#define Tip2_SetDigitalOutput()   do { TRISAbits.TRISA7 = 0; } while(0)
#define Tip2_SetPullup()      do { WPUAbits.WPUA7 = 1; } while(0)
#define Tip2_ResetPullup()    do { WPUAbits.WPUA7 = 0; } while(0)
#define Tip2_SetPushPull()    do { ODCONAbits.ODA7 = 1; } while(0)
#define Tip2_SetOpenDrain()   do { ODCONAbits.ODA7 = 0; } while(0)

// get/set CAN_INT aliases
#define CAN_INT_TRIS               TRISBbits.TRISB0
#define CAN_INT_LAT                LATBbits.LATB0
#define CAN_INT_PORT               PORTBbits.RB0
#define CAN_INT_WPU                WPUBbits.WPUB0
#define CAN_INT_OD                ODCONBbits.ODB0
#define CAN_INT_ANS                ANSELBbits.ANSB0
#define CAN_INT_SetHigh()            do { LATBbits.LATB0 = 1; } while(0)
#define CAN_INT_SetLow()             do { LATBbits.LATB0 = 0; } while(0)
#define CAN_INT_Toggle()             do { LATBbits.LATB0 = ~LATBbits.LATB0; } while(0)
#define CAN_INT_GetValue()           PORTBbits.RB0
#define CAN_INT_SetDigitalInput()    do { TRISBbits.TRISB0 = 1; } while(0)
#define CAN_INT_SetDigitalOutput()   do { TRISBbits.TRISB0 = 0; } while(0)
#define CAN_INT_SetPullup()      do { WPUBbits.WPUB0 = 1; } while(0)
#define CAN_INT_ResetPullup()    do { WPUBbits.WPUB0 = 0; } while(0)
#define CAN_INT_SetPushPull()    do { ODCONBbits.ODB0 = 1; } while(0)
#define CAN_INT_SetOpenDrain()   do { ODCONBbits.ODB0 = 0; } while(0)
#define CAN_INT_SetAnalogMode()  do { ANSELBbits.ANSB0 = 1; } while(0)
#define CAN_INT_SetDigitalMode() do { ANSELBbits.ANSB0 = 0; } while(0)

// get/set TiltNormal aliases
#define TiltNormal_TRIS               TRISBbits.TRISB1
#define TiltNormal_LAT                LATBbits.LATB1
#define TiltNormal_PORT               PORTBbits.RB1
#define TiltNormal_WPU                WPUBbits.WPUB1
#define TiltNormal_OD                ODCONBbits.ODB1
#define TiltNormal_ANS                ANSELBbits.ANSB1
#define TiltNormal_SetHigh()            do { LATBbits.LATB1 = 1; } while(0)
#define TiltNormal_SetLow()             do { LATBbits.LATB1 = 0; } while(0)
#define TiltNormal_Toggle()             do { LATBbits.LATB1 = ~LATBbits.LATB1; } while(0)
#define TiltNormal_GetValue()           PORTBbits.RB1
#define TiltNormal_SetDigitalInput()    do { TRISBbits.TRISB1 = 1; } while(0)
#define TiltNormal_SetDigitalOutput()   do { TRISBbits.TRISB1 = 0; } while(0)
#define TiltNormal_SetPullup()      do { WPUBbits.WPUB1 = 1; } while(0)
#define TiltNormal_ResetPullup()    do { WPUBbits.WPUB1 = 0; } while(0)
#define TiltNormal_SetPushPull()    do { ODCONBbits.ODB1 = 1; } while(0)
#define TiltNormal_SetOpenDrain()   do { ODCONBbits.ODB1 = 0; } while(0)
#define TiltNormal_SetAnalogMode()  do { ANSELBbits.ANSB1 = 1; } while(0)
#define TiltNormal_SetDigitalMode() do { ANSELBbits.ANSB1 = 0; } while(0)

// get/set An3 aliases
#define An3_TRIS               TRISBbits.TRISB3
#define An3_LAT                LATBbits.LATB3
#define An3_PORT               PORTBbits.RB3
#define An3_WPU                WPUBbits.WPUB3
#define An3_OD                ODCONBbits.ODB3
#define An3_ANS                ANSELBbits.ANSB3
#define An3_SetHigh()            do { LATBbits.LATB3 = 1; } while(0)
#define An3_SetLow()             do { LATBbits.LATB3 = 0; } while(0)
#define An3_Toggle()             do { LATBbits.LATB3 = ~LATBbits.LATB3; } while(0)
#define An3_GetValue()           PORTBbits.RB3
#define An3_SetDigitalInput()    do { TRISBbits.TRISB3 = 1; } while(0)
#define An3_SetDigitalOutput()   do { TRISBbits.TRISB3 = 0; } while(0)
#define An3_SetPullup()      do { WPUBbits.WPUB3 = 1; } while(0)
#define An3_ResetPullup()    do { WPUBbits.WPUB3 = 0; } while(0)
#define An3_SetPushPull()    do { ODCONBbits.ODB3 = 1; } while(0)
#define An3_SetOpenDrain()   do { ODCONBbits.ODB3 = 0; } while(0)
#define An3_SetAnalogMode()  do { ANSELBbits.ANSB3 = 1; } while(0)
#define An3_SetDigitalMode() do { ANSELBbits.ANSB3 = 0; } while(0)

// get/set An2 aliases
#define An2_TRIS               TRISBbits.TRISB4
#define An2_LAT                LATBbits.LATB4
#define An2_PORT               PORTBbits.RB4
#define An2_WPU                WPUBbits.WPUB4
#define An2_OD                ODCONBbits.ODB4
#define An2_ANS                ANSELBbits.ANSB4
#define An2_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define An2_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define An2_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define An2_GetValue()           PORTBbits.RB4
#define An2_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define An2_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define An2_SetPullup()      do { WPUBbits.WPUB4 = 1; } while(0)
#define An2_ResetPullup()    do { WPUBbits.WPUB4 = 0; } while(0)
#define An2_SetPushPull()    do { ODCONBbits.ODB4 = 1; } while(0)
#define An2_SetOpenDrain()   do { ODCONBbits.ODB4 = 0; } while(0)
#define An2_SetAnalogMode()  do { ANSELBbits.ANSB4 = 1; } while(0)
#define An2_SetDigitalMode() do { ANSELBbits.ANSB4 = 0; } while(0)

// get/set An1 aliases
#define An1_TRIS               TRISBbits.TRISB5
#define An1_LAT                LATBbits.LATB5
#define An1_PORT               PORTBbits.RB5
#define An1_WPU                WPUBbits.WPUB5
#define An1_OD                ODCONBbits.ODB5
#define An1_ANS                ANSELBbits.ANSB5
#define An1_SetHigh()            do { LATBbits.LATB5 = 1; } while(0)
#define An1_SetLow()             do { LATBbits.LATB5 = 0; } while(0)
#define An1_Toggle()             do { LATBbits.LATB5 = ~LATBbits.LATB5; } while(0)
#define An1_GetValue()           PORTBbits.RB5
#define An1_SetDigitalInput()    do { TRISBbits.TRISB5 = 1; } while(0)
#define An1_SetDigitalOutput()   do { TRISBbits.TRISB5 = 0; } while(0)
#define An1_SetPullup()      do { WPUBbits.WPUB5 = 1; } while(0)
#define An1_ResetPullup()    do { WPUBbits.WPUB5 = 0; } while(0)
#define An1_SetPushPull()    do { ODCONBbits.ODB5 = 1; } while(0)
#define An1_SetOpenDrain()   do { ODCONBbits.ODB5 = 0; } while(0)
#define An1_SetAnalogMode()  do { ANSELBbits.ANSB5 = 1; } while(0)
#define An1_SetDigitalMode() do { ANSELBbits.ANSB5 = 0; } while(0)

// get/set ReachIn aliases
#define ReachIn_TRIS               TRISBbits.TRISB6
#define ReachIn_LAT                LATBbits.LATB6
#define ReachIn_PORT               PORTBbits.RB6
#define ReachIn_WPU                WPUBbits.WPUB6
#define ReachIn_OD                ODCONBbits.ODB6
#define ReachIn_SetHigh()            do { LATBbits.LATB6 = 1; } while(0)
#define ReachIn_SetLow()             do { LATBbits.LATB6 = 0; } while(0)
#define ReachIn_Toggle()             do { LATBbits.LATB6 = ~LATBbits.LATB6; } while(0)
#define ReachIn_GetValue()           PORTBbits.RB6
#define ReachIn_SetDigitalInput()    do { TRISBbits.TRISB6 = 1; } while(0)
#define ReachIn_SetDigitalOutput()   do { TRISBbits.TRISB6 = 0; } while(0)
#define ReachIn_SetPullup()      do { WPUBbits.WPUB6 = 1; } while(0)
#define ReachIn_ResetPullup()    do { WPUBbits.WPUB6 = 0; } while(0)
#define ReachIn_SetPushPull()    do { ODCONBbits.ODB6 = 1; } while(0)
#define ReachIn_SetOpenDrain()   do { ODCONBbits.ODB6 = 0; } while(0)

// get/set ReachOut aliases
#define ReachOut_TRIS               TRISBbits.TRISB7
#define ReachOut_LAT                LATBbits.LATB7
#define ReachOut_PORT               PORTBbits.RB7
#define ReachOut_WPU                WPUBbits.WPUB7
#define ReachOut_OD                ODCONBbits.ODB7
#define ReachOut_SetHigh()            do { LATBbits.LATB7 = 1; } while(0)
#define ReachOut_SetLow()             do { LATBbits.LATB7 = 0; } while(0)
#define ReachOut_Toggle()             do { LATBbits.LATB7 = ~LATBbits.LATB7; } while(0)
#define ReachOut_GetValue()           PORTBbits.RB7
#define ReachOut_SetDigitalInput()    do { TRISBbits.TRISB7 = 1; } while(0)
#define ReachOut_SetDigitalOutput()   do { TRISBbits.TRISB7 = 0; } while(0)
#define ReachOut_SetPullup()      do { WPUBbits.WPUB7 = 1; } while(0)
#define ReachOut_ResetPullup()    do { WPUBbits.WPUB7 = 0; } while(0)
#define ReachOut_SetPushPull()    do { ODCONBbits.ODB7 = 1; } while(0)
#define ReachOut_SetOpenDrain()   do { ODCONBbits.ODB7 = 0; } while(0)

// get/set TiltUp aliases
#define TiltUp_TRIS               TRISCbits.TRISC1
#define TiltUp_LAT                LATCbits.LATC1
#define TiltUp_PORT               PORTCbits.RC1
#define TiltUp_WPU                WPUCbits.WPUC1
#define TiltUp_OD                ODCONCbits.ODC1
#define TiltUp_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define TiltUp_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define TiltUp_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define TiltUp_GetValue()           PORTCbits.RC1
#define TiltUp_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define TiltUp_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)
#define TiltUp_SetPullup()      do { WPUCbits.WPUC1 = 1; } while(0)
#define TiltUp_ResetPullup()    do { WPUCbits.WPUC1 = 0; } while(0)
#define TiltUp_SetPushPull()    do { ODCONCbits.ODC1 = 1; } while(0)
#define TiltUp_SetOpenDrain()   do { ODCONCbits.ODC1 = 0; } while(0)

// get/set LED aliases
#define LED_TRIS               TRISCbits.TRISC2
#define LED_LAT                LATCbits.LATC2
#define LED_PORT               PORTCbits.RC2
#define LED_WPU                WPUCbits.WPUC2
#define LED_OD                ODCONCbits.ODC2
#define LED_ANS                ANSELCbits.ANSC2
#define LED_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define LED_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define LED_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define LED_GetValue()           PORTCbits.RC2
#define LED_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define LED_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define LED_SetPullup()      do { WPUCbits.WPUC2 = 1; } while(0)
#define LED_ResetPullup()    do { WPUCbits.WPUC2 = 0; } while(0)
#define LED_SetPushPull()    do { ODCONCbits.ODC2 = 1; } while(0)
#define LED_SetOpenDrain()   do { ODCONCbits.ODC2 = 0; } while(0)
#define LED_SetAnalogMode()  do { ANSELCbits.ANSC2 = 1; } while(0)
#define LED_SetDigitalMode() do { ANSELCbits.ANSC2 = 0; } while(0)

// get/set RC3 procedures
#define RC3_SetHigh()    do { LATCbits.LATC3 = 1; } while(0)
#define RC3_SetLow()   do { LATCbits.LATC3 = 0; } while(0)
#define RC3_Toggle()   do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define RC3_GetValue()         PORTCbits.RC3
#define RC3_SetDigitalInput()   do { TRISCbits.TRISC3 = 1; } while(0)
#define RC3_SetDigitalOutput()  do { TRISCbits.TRISC3 = 0; } while(0)
#define RC3_SetPullup()     do { WPUCbits.WPUC3 = 1; } while(0)
#define RC3_ResetPullup()   do { WPUCbits.WPUC3 = 0; } while(0)
#define RC3_SetAnalogMode() do { ANSELCbits.ANSC3 = 1; } while(0)
#define RC3_SetDigitalMode()do { ANSELCbits.ANSC3 = 0; } while(0)

// get/set RC4 procedures
#define RC4_SetHigh()    do { LATCbits.LATC4 = 1; } while(0)
#define RC4_SetLow()   do { LATCbits.LATC4 = 0; } while(0)
#define RC4_Toggle()   do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define RC4_GetValue()         PORTCbits.RC4
#define RC4_SetDigitalInput()   do { TRISCbits.TRISC4 = 1; } while(0)
#define RC4_SetDigitalOutput()  do { TRISCbits.TRISC4 = 0; } while(0)
#define RC4_SetPullup()     do { WPUCbits.WPUC4 = 1; } while(0)
#define RC4_ResetPullup()   do { WPUCbits.WPUC4 = 0; } while(0)
#define RC4_SetAnalogMode() do { ANSELCbits.ANSC4 = 1; } while(0)
#define RC4_SetDigitalMode()do { ANSELCbits.ANSC4 = 0; } while(0)

// get/set CS aliases
#define CS_TRIS               TRISCbits.TRISC5
#define CS_LAT                LATCbits.LATC5
#define CS_PORT               PORTCbits.RC5
#define CS_WPU                WPUCbits.WPUC5
#define CS_OD                ODCONCbits.ODC5
#define CS_ANS                ANSELCbits.ANSC5
#define CS_SetHigh()            do { LATCbits.LATC5 = 1; } while(0)
#define CS_SetLow()             do { LATCbits.LATC5 = 0; } while(0)
#define CS_Toggle()             do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define CS_GetValue()           PORTCbits.RC5
#define CS_SetDigitalInput()    do { TRISCbits.TRISC5 = 1; } while(0)
#define CS_SetDigitalOutput()   do { TRISCbits.TRISC5 = 0; } while(0)
#define CS_SetPullup()      do { WPUCbits.WPUC5 = 1; } while(0)
#define CS_ResetPullup()    do { WPUCbits.WPUC5 = 0; } while(0)
#define CS_SetPushPull()    do { ODCONCbits.ODC5 = 1; } while(0)
#define CS_SetOpenDrain()   do { ODCONCbits.ODC5 = 0; } while(0)
#define CS_SetAnalogMode()  do { ANSELCbits.ANSC5 = 1; } while(0)
#define CS_SetDigitalMode() do { ANSELCbits.ANSC5 = 0; } while(0)

// get/set RC6 procedures
#define RC6_SetHigh()    do { LATCbits.LATC6 = 1; } while(0)
#define RC6_SetLow()   do { LATCbits.LATC6 = 0; } while(0)
#define RC6_Toggle()   do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define RC6_GetValue()         PORTCbits.RC6
#define RC6_SetDigitalInput()   do { TRISCbits.TRISC6 = 1; } while(0)
#define RC6_SetDigitalOutput()  do { TRISCbits.TRISC6 = 0; } while(0)
#define RC6_SetPullup()     do { WPUCbits.WPUC6 = 1; } while(0)
#define RC6_ResetPullup()   do { WPUCbits.WPUC6 = 0; } while(0)
#define RC6_SetAnalogMode() do { ANSELCbits.ANSC6 = 1; } while(0)
#define RC6_SetDigitalMode()do { ANSELCbits.ANSC6 = 0; } while(0)

// get/set RC7 procedures
#define RC7_SetHigh()    do { LATCbits.LATC7 = 1; } while(0)
#define RC7_SetLow()   do { LATCbits.LATC7 = 0; } while(0)
#define RC7_Toggle()   do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define RC7_GetValue()         PORTCbits.RC7
#define RC7_SetDigitalInput()   do { TRISCbits.TRISC7 = 1; } while(0)
#define RC7_SetDigitalOutput()  do { TRISCbits.TRISC7 = 0; } while(0)
#define RC7_SetPullup()     do { WPUCbits.WPUC7 = 1; } while(0)
#define RC7_ResetPullup()   do { WPUCbits.WPUC7 = 0; } while(0)
#define RC7_SetAnalogMode() do { ANSELCbits.ANSC7 = 1; } while(0)
#define RC7_SetDigitalMode()do { ANSELCbits.ANSC7 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/