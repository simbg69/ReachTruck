/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using MPLAB(c) Code Configurator

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB(c) Code Configurator - 3.16
        Device            :  PIC16F1716
        Driver Version    :  2.00
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.35
        MPLAB             :  MPLAB X 3.20
*/

/* ERROR CODE:
 * +-----+---------------------------------------------------------------------+
 * | BIT | Description                                                         |
 * +-----+---------------------------------------------------------------------+
 * |  0  | Lift Encoder Error                                                  |
 * |  1  | Reach Encoder Error                                                 |
 * |  2  | Reach Timeout                                                       |
 * |  3  | Lift Timeout                                                        |
 * |  4  | Find Hole Timeout                                                   |
 * +-----+---------------------------------------------------------------------+
 */

#include "mcc_generated_files/mcc.h"
#include "bareMetal.h"

#define ABS(x) ((x) > 0 ? (x) : -(x))

#define MY_ADDR 0x3
#define MY_ADDR2 0

//Lift Use
bool LiftFlag =1;
bool LiftStatus=0;
unsigned int *TargetADC = 0x1000 ;
unsigned int *HeightADC;
unsigned int *ReachADC;
uint8_t *Status;
uint8_t hole_counter =0;

void LiftUp();
void LiftDown();
void LiftStop();
void ReachExtend();
void ReachRetract();
void ReachStop();
void ForkTilt();
void ForkNormal();
void TiltStop();

void GotoPosition();
void find_hole();

void start_find_hole();

// CAN Use
CAN_Data Report_Data;
static uint8_t tip_status = 0;
static bool finding_hole = 0;

void device_specific_command(CAN_Data buff);

/*V2*/
uint16_t lift_height_adc;
uint16_t reach_adc;
bool tip_sensor[2];
bool lift_moving = false;
bool reach_moving = false;
bool tilted = false;
bool reach_extended = false;
bool reach_retracted = false;
uint8_t reach_action = 0;
uint8_t ID_REACH;
uint8_t ID_GOTO;
uint16_t timeout_cnt = 0;
bool success = 0;

uint16_t average16(uint16_t *a);
void ProcessSensor();
void CheckSensor();
void reach_move();

void delay_xms(unsigned long x);

void putStr(char *txStr)
{
    while (*txStr != 0)
    {
        putch(*txStr);
        txStr++;
    }
}

void uartPrintVersion()
{
    char version[16];
    sprintf(version, "%s", "Lift v0.4.0\n\r");
    putStr(version);
}

void main(void){
    SYSTEM_Initialize();
    init_bareMetal(MY_ADDR,MY_ADDR2);
    Report_data_ptr = &Report_Data;
    HeightADC = &Report_Data.data[2];
    TargetADC = &Report_Data.data[4];
    ReachADC = &Report_Data.data[6];
    Status = &Report_Data.data[1];
    Report_Data.len = 8;
    SetCommandCallBack(device_specific_command);
    
    LiftStop();
    ReachStop();
    TiltStop();
            
    register_thread(1,ProcessSensor);
    //register_thread(1000,CheckSensor);
    ID_GOTO = register_thread(EEDATA[GOTO_REFRESH_RATE],GotoPosition);
    //register_thread(EEDATA[FINDHOLE_REFRESH_RATE],find_hole);
    //ID_REACH = register_thread(20,reach_move);
    //thread_pause(ID_REACH);
    thread_pause(ID_GOTO);
    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();
    
    SWDTEN = 1;
    
    //uartPrintVersion();
        
    while (1)
    {
        thread_update();
        Received_Command();
        
        //uartPrintVersion();
    }
}

void device_specific_command(CAN_Data buff){
    switch(buff.data[0]){
        case 0:
            LiftUp();
            break;
        case 1:
            LiftDown();
            break;
        case 2:
            LiftStop();
            finding_hole=0;
            hole_counter =0;
            thread_pause(ID_GOTO);
            error_code = 0;
            success = 0;//20180118: Attempt to fix "success prematurely sent to Ant" issue.
            break;          
        case 3:  //goto position;
            //error_code = 0;
            *TargetADC=(buff.data[1]<<8)|buff.data[2];
            reach_action = buff.data[3];
            timeout_cnt = 0;
            error_code = 0;
            success = 0;            
            thread_resume(ID_GOTO);
            //beng: moved to before resuming thread
            //timeout_cnt = 0;
            //success = 0;
            break;   
        case 4:
            thread_pause(ID_GOTO);
            LiftFlag=0;
            hole_counter=0;
            start_find_hole();
            break;
        //simbg 20180105: Prevent Lift firmware from handling separate reach command other than those combined with height in Case 3 above.
        //simbg 20180213: Revert back as needed for joystick mode.
        case 5:
            ReachExtend();
            break;
        case 6:
            ReachRetract();
            break;
        case 7:
            ReachStop();
            break;
        //beng 20180105: Prevent Lift firmware from handling separate reach command other than those combined with height in Case 3 above (WHEN guided by BlueBotics ANT)       
        //case 8:
        //    success = 0;
        //    reach_action = buff.data[1];
        //    thread_resume(ID_REACH);
        //    break;
        case 9:
            ForkTilt();
            break;
        case 10:
            ForkNormal();
            break;            
    }        
}

void LiftUp(){
    Lift_Down_SetHigh();
    Lift_Up_SetLow();
}
void LiftDown(){
    Lift_Up_SetHigh();
    Lift_Down_SetLow();
}
void LiftStop(){
    Lift_Up_SetHigh();
    Lift_Down_SetHigh();
}
void ReachStop(){
    ReachOut_SetHigh();
    ReachIn_SetHigh();
}
void ReachExtend(){
    //some where the pin from the reach truck's ReachExtend and ReachRetract 
    //pin is wrong
    //ReachOut_SetLow();
    //ReachIn_SetHigh();
    ReachOut_SetHigh();
    ReachIn_SetLow();
    
}
void ReachRetract(){
    //some where the pin from the reach truck's ReachExtend and ReachRetract 
    //pin is wrong
    //
    //ReachOut_SetHigh();
    //ReachIn_SetLow();
    ReachOut_SetLow();
    ReachIn_SetHigh();
}

void TiltStop(){
    TiltUp_SetHigh();
    TiltNormal_SetHigh();
}

void ForkTilt(){
    unsigned long tilt_delay = 0;
    thread_pause(ID_GOTO);/// = register_thread(EEDATA[GOTO_REFRESH_RATE],GotoPosition);
    //register_thread(EEDATA[FINDHOLE_REFRESH_RATE],find_hole);
    //thread_pause(ID_REACH);
    LiftStop();
    ReachStop();
    TiltUp_SetLow();
    TiltNormal_SetHigh();
    //tilt_delay = (EEDATA[TILTNORMAL_TIME]==0xFFFF) ? 0 : (EEDATA[TILTNORMAL_TIME] * 10);
    tilt_delay = (EEDATA[TILTUP_TIME]==0xFFFF) ? 0 : (EEDATA[TILTUP_TIME] * 10);
    delay_xms(tilt_delay);
    //__delay_ms((EEDATA[TILTUP_TIME] ==0xFFFF)?0:EEDATA[TILTUP_TIME]);
    TiltStop();
}
void ForkNormal(){
    unsigned long tilt_delay = 0;
    thread_pause(ID_GOTO);/// = register_thread(EEDATA[GOTO_REFRESH_RATE],GotoPosition);
    //register_thread(EEDATA[FINDHOLE_REFRESH_RATE],find_hole);
    //thread_pause(ID_REACH);
    LiftStop();
    ReachStop();
    TiltUp_SetHigh();
    TiltNormal_SetLow();
    //delay_ms = 1000;//(EEDATA[TILTNORMAL_TIME]==0xFFFF) ? 0 : (EEDATA[TILTNORMAL_TIME] * 10);
    tilt_delay = (EEDATA[TILTNORMAL_TIME]==0xFFFF) ? 0 : (EEDATA[TILTNORMAL_TIME] * 10);
    delay_xms(tilt_delay);
    TiltStop();
}

void start_find_hole(){
    tip_status = 0;
    finding_hole = 1;
    LiftFlag = 0;
    LiftDown();
}
void find_hole(){
    if (Tip1_GetValue()==0)
        tip_status |= 1;
    if (Tip2_GetValue()==0)
        tip_status |= 2;
    if (tip_status == 3 && finding_hole ==1)
        hole_counter++;
    // Counter == 10, thread enter every 10ms, total will be 100ms
    if (tip_status == 3 && finding_hole == 1 & hole_counter > 90){
       LiftStop();
       finding_hole = 0;
       tip_status = 0;
       hole_counter = 0;
       LiftFlag = 1;
    }     
}

typedef enum { HEIGHT, EXTEND, RETRACT } ActionType;

int CheckReachMove(ActionType actionType){
    
    if (actionType == RETRACT){
        if (reach_retracted == 1){
            ReachStop();
            return 1;
            //reach_move_state = 2;
        }
    } else if(actionType == EXTEND) {
        if (reach_extended == 1){
            ReachStop();
            return 1;
            //reach_move_state = 2;
        }
    }
    else{
        return 1;
        //reach_move_state = 2;
    }
    
    return 0; 
    
//    //static uint16_t counter = 0;
//    //counter++;
//    if (actionType == RETRACT){
//        if (reach_retracted == 1){
//            ReachStop();
//            //success = 1;
//            //counter = 0;
//            //thread_pause(ID_REACH);
//        }
//        else if (reach_moving == 0){
//            ReachRetract();
//        }
//
//    } else {
//        if (reach_extended == EXTEND){
//            ReachStop();
//            //success = 1;
//            //counter = 0;
//            //thread_pause(ID_REACH);
//        }
//        else if (reach_moving == 0){
//            ReachExtend();
//        }
//    }
//    
////    if ((reach_action == 1) && (reach_moving == 0) && (reach_extended == 1)){
////        if ((Tip1_GetValue()==0) || (Tip2_GetValue()==0)){
////            //thread_pause(ID_REACH);
////            ReachStop();
////            counter = EEDATA[REACH_TIMEOUT];
////            reach_error_code |= 0x16;    
////        }
////    }
////    
////    if(counter > EEDATA[REACH_TIMEOUT]){
////        //thread_pause(ID_REACH);
////        ReachStop();
////        counter = EEDATA[REACH_TIMEOUT];
////        reach_error_code |= 0x4;
////    }
}

static uint8_t height_move_status =0;

uint8_t CheckHeightMove()
{
    return height_move_status;
}

void ResetHeightMove()
{
    height_move_status = 0;
}


//void GotoPosition(){
void GotoHeightPosition(){
    static uint16_t confidence = 0;
    static uint16_t move_counter = 0;
    static bool stopMove = 1;
    timeout_cnt++;
    long h = ((long)*HeightADC - (long)*TargetADC);
    if (ABS(h) >= EEDATA[GROSS_TUNE]){
        timeout_cnt--;
        stopMove = 1;
        confidence = 0;
        if(h>0)
            LiftDown();
        if(h<0)
            LiftUp();
    }
//    else if(ABS(h)>EEDATA[FINE_TUNE]){
//        confidence = 0;
//        if(stopMove == 1){
//            LiftStop();
//            if(move_counter++ > EEDATA[FORK_STOP_TIME]){
//                stopMove = 0;
//                move_counter = 0;
//            }
//        } else{
//            if(h>0)
//                LiftDown();
//            else if(h<0)
//                LiftUp();
//            if(move_counter++ > EEDATA[FORK_MOVE_TIME]){
//                stopMove = 1;
//                move_counter = 0;
//            }
//        }
//    } 
    else{
        LiftStop();
        if(confidence++ > EEDATA[CONFIDENCE_CNT]){
            if(*TargetADC < EEDATA[GROUNDOFFSET]){
                LiftDown();
                __delay_ms(1000);
            }
            LiftStop();
            move_counter = 0;
            //thread_pause(ID_GOTO);
            //success = 1;
            
            height_move_status = 1;
        }
    }
    

//    if (timeout_cnt >= EEDATA[GOTO_TIMEOUT]){
//        LiftStop();
//        thread_pause(ID_GOTO);
//        error_code |= 0x8;
//    }
}

enum
{
    SENSOR_DISABLED    = 0b00,
    SENSOR_ENABLED_1   = 0b01,
    SENSOR_ENABLED_2   = 0b10,
    SENSOR_ENABLED_ALL = 0b11,
} UnloadPalletSensorState;

void UnloadPallet()
{
    if(UnloadPalletSensorState & SENSOR_ENABLED_1)
    {
        if(!Tip1_GetValue())
        {
            UnloadPalletSensorState = SENSOR_DISABLED;
            *TargetADC = *HeightADC - EEDATA[FINE_TUNE];
        }
    }
    if(UnloadPalletSensorState & SENSOR_ENABLED_2)
    {
        if(!Tip2_GetValue())
        {
            UnloadPalletSensorState = SENSOR_DISABLED;
            *TargetADC = *HeightADC - EEDATA[FINE_TUNE];
        }
    }
    GotoHeightPosition();
}

typedef struct GotoPositionStep
{
    ActionType actionType;
    void (*actionFunc)(void);
} GotoPositionStep;

GotoPositionStep gotoPositionStep[2] = {};
//reach_move_state = 0;
void GotoPosition() {  

    static uint8_t reach_move_state = 0;

    switch (reach_move_state) {
        case 0:
        {
            ResetHeightMove();
            
            //20180209 simbg:
            //Action 1 currently used in ANT to do pallet layer 2 pickup
            //  using ifm 3d sensor pallet hole seeking
            //  by setting ifm_seek_pocket.reach_action (reach_action_to_trigger_pallet_seek) = 1
            if (reach_action == 1)
            {
                gotoPositionStep[0].actionFunc = GotoHeightPosition;
                gotoPositionStep[0].actionType = HEIGHT;
                
                gotoPositionStep[1].actionFunc = ReachExtend;
                gotoPositionStep[1].actionType = EXTEND;
            }
            else if (reach_action == 2)
            {
                gotoPositionStep[0].actionFunc = GotoHeightPosition;
                gotoPositionStep[0].actionType = HEIGHT;
                
                gotoPositionStep[1].actionFunc = ReachRetract;
                gotoPositionStep[1].actionType = RETRACT;
            }
            //20180209 simbg:
            //3 for actual use by ANT to move fork up/down without reach extend/retract;
            //9 for TESTING ifm 3d sensor pallet hole seeking without reach extend/retract
            //  by setting ifm_seek_pocket.reach_action (reach_action_to_trigger_pallet_seek) = 9
            else if (reach_action == 3 || reach_action == 9)
            {
                gotoPositionStep[0].actionFunc = GotoHeightPosition;
                gotoPositionStep[0].actionType = HEIGHT;
                
                gotoPositionStep[1].actionFunc = 0;
                gotoPositionStep[1].actionType = 0;
            }
            else if (reach_action == 4)
            {
                gotoPositionStep[0].actionFunc = ReachExtend;
                gotoPositionStep[0].actionType = EXTEND;
                
                gotoPositionStep[1].actionFunc = GotoHeightPosition;
                gotoPositionStep[1].actionType = HEIGHT;
            }
            else if (reach_action == 5)
            {
                gotoPositionStep[0].actionFunc = ReachRetract;
                gotoPositionStep[0].actionType = RETRACT;
                
                gotoPositionStep[1].actionFunc = GotoHeightPosition;
                gotoPositionStep[1].actionType = HEIGHT;
            }
            else if (reach_action == 6)
            {
                gotoPositionStep[0].actionFunc = ReachExtend;
                gotoPositionStep[0].actionType = EXTEND;            
                
                gotoPositionStep[1].actionFunc = 0;
                gotoPositionStep[1].actionType = 0;
            }
            else if (reach_action == 7)
            {
                gotoPositionStep[0].actionFunc = ReachRetract;
                gotoPositionStep[0].actionType = RETRACT;
                
                gotoPositionStep[1].actionFunc = 0;
                gotoPositionStep[1].actionType = 0;
            }
            else if (reach_action == 8)
            {
                UnloadPalletSensorState = SENSOR_DISABLED;
                if(Tip1_GetValue())
                {
                    UnloadPalletSensorState |= SENSOR_ENABLED_1;
                }
                if(Tip2_GetValue())
                {
                    UnloadPalletSensorState |= SENSOR_ENABLED_2;
                }
                gotoPositionStep[0].actionFunc = UnloadPallet;
                gotoPositionStep[0].actionType = HEIGHT;
                
                gotoPositionStep[1].actionFunc = ReachRetract;
                gotoPositionStep[1].actionType = RETRACT;
            }            
            //else if (reach_action == 9) //with action 3 above
            //else
            //{
            //}
            reach_move_state = 1;
            break;
        }        
        case 1:
        {
            gotoPositionStep[0].actionFunc();
            if (    (gotoPositionStep[0].actionType == EXTEND   )
                ||  (gotoPositionStep[0].actionType == RETRACT  )  )
            {
                if (CheckReachMove(gotoPositionStep[0].actionType))
                {
                    reach_move_state = 2;
                }
            }
            else //if (    (gotoPositionStep[0].actionType == HEIGHT   )
            {
                if (CheckHeightMove())
                {
                    reach_move_state = 2;
                }
            }
            break;
        }
        case 2:
        {
            if (gotoPositionStep[1].actionFunc != 0)
            {
                gotoPositionStep[1].actionFunc();
                if (    (gotoPositionStep[1].actionType == EXTEND   )
                    ||  (gotoPositionStep[1].actionType == RETRACT  )  )
                {
                    if (CheckReachMove(gotoPositionStep[1].actionType))
                    {
                        reach_move_state = 3;
                    }
                }
                 else //if (    (gotoPositionStep[0].actionType == HEIGHT   )
                {
                    if (CheckHeightMove())
                    {
                        reach_move_state = 3;
                    }
                }
            }
            else
            {
                reach_move_state = 3;
            }
            break;
        }
        case 3:
        {
            thread_pause(ID_GOTO);
            success = 1;
            reach_move_state = 0;
            break;
        }
        default:
        {
            thread_pause(ID_GOTO);
            reach_move_state = 0;
            break;
        }
    }
}

//void reach_move(){
//    static uint16_t counter = 0;
//    counter++;
//    if (reach_action == 0){
//        if (reach_retracted == 1){
//            ReachStop();
//            success = 1;
//            counter = 0;
//            thread_pause(ID_REACH);
//        }
//        else if (reach_moving == 0){
//            ReachRetract();
//        }
//
//    } else {
//        if (reach_extended == 1){
//            ReachStop();
//            success = 1;
//            counter = 0;
//            thread_pause(ID_REACH);
//        }
//        else if (reach_moving == 0){
//            ReachExtend();
//        }
//    }
//    
//    if ((reach_action == 1) && (reach_moving == 0) && (reach_extended == 1)){
//        if ((Tip1_GetValue()==0) || (Tip2_GetValue()==0)){
//            thread_pause(ID_REACH);
//            ReachStop();
//            counter = EEDATA[REACH_TIMEOUT];
//            error_code |= 0x16;    
//        }
//    }
//    
//    if(counter > EEDATA[REACH_TIMEOUT]){
//        thread_pause(ID_REACH);
//        ReachStop();
//        counter = EEDATA[REACH_TIMEOUT];
//        error_code |= 0x4;
//    }
//}
void ProcessSensor(){
    static uint16_t height_adc_running[16];
    static uint16_t reach_adc_running[16];
    static uint8_t running_index = 0;
    static uint16_t prev_height_adc;
    static uint16_t prev_reach_adc;
    
    height_adc_running[running_index] = ADC_GetConversion(An2);
    reach_adc_running[running_index] = ADC_GetConversion(An1);
    *HeightADC = average16(height_adc_running);
    *ReachADC = average16(reach_adc_running);
    
    lift_moving = ((int)ABS((prev_height_adc - *HeightADC)) < 5)?false:true;
    reach_moving = ((int)ABS(prev_reach_adc - *ReachADC) < 5)?false:true;
    
    reach_extended = (*ReachADC > (unsigned int)EEDATA[REACH_THRES_OUT])?1:0;
    reach_retracted = (*ReachADC < (unsigned int)EEDATA[REACH_THRES_IN])?1:0;
    prev_height_adc = *HeightADC;
    prev_reach_adc = *ReachADC;
    
    if(++running_index>=16)
        running_index = 0;
    *Status = scheduler_thread[ID_REACH].start<<4| 
            scheduler_thread[ID_GOTO].start<<4|success<<3|
            reach_moving<<2|lift_moving<<2|Tip2_GetValue()<<1|Tip1_GetValue();
}

uint16_t average16(uint16_t *a){
    long ans = 0;
    for (int i =0; i < 16; i++){
        ans += (*a++);
    }
    return ans>>4;
}

//void CheckSensor(){
//    static uint16_t prev_height_sensor;
//    static uint16_t prev_reach_sensor;
//    static uint8_t counter,counter1;
//    
//    if (Lift_Up_GetValue()==0 || Lift_Down_GetValue()==0){
//        counter++;
//        if((int)ABS(prev_height_sensor - *HeightADC)<100 && counter > EEDATA[SENSOR_CHECK_TIMEOUT]){
//            error_code |= 0x1;
//        }
//    } else{
//        counter = 0;
//        error_code &= 0xFE;
//    }
//    if (ReachOut_GetValue()==0 || ReachIn_GetValue()==0){
//        counter1++;
//        if((int)ABS(prev_reach_sensor - *ReachADC)<100&& counter1 >EEDATA[SENSOR_CHECK_TIMEOUT]){
//            error_code |= 0x2;
//        }
//    }else{
//        counter1 =0;
//        error_code &= 0xFD;
//    }
//    prev_height_sensor = *HeightADC;
//    prev_reach_sensor = *ReachADC;
//    
//    //TODO: BUG!!! This won't stop, and can't halt thread here!
//    if (error_code){
//        LiftStop();
//    }
//}

void delay_xms(unsigned long x){
    SWDTEN = 0;
    for(x; x>0; x--){
        __delay_ms(1);
    }
    SWDTEN = 1;
}
