/* 
 * File:   statusLED.h
 * Author: Jonathan
 *
 * Created on October 26, 2016, 9:51 PM
 */

#ifndef STATUSLED_H
#define	STATUSLED_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <xc.h>
#include "mcc_generated_files/mcc.h"

#define STATUS_LED_DARK 10

uint8_t LED_status = 2;
void status_LED();

#ifdef	__cplusplus
}
#endif

#endif	/* STATUSLED_H */

