/*
 * File:   mcp2515.c
 * Author: Jonathan
 *
 * Created on October 26, 2016, 2:17 PM
 */

#include "mcp2515.h"
#include "mcc_generated_files/mcc.h"

void cc_reset(void){
    uint8_t data[1] = {MCP_RESET};
    MCP_write(data,1);
}

void cc_init(uint8_t PS, uint16_t MY_ADDR,uint16_t MY_ADDR2){
    //Change MCP Mode to Config
    cc_setMode(MODE_CONFIG);
    //SET CNF to 250KBPS, using 20MHz Crystal
    MCP_write(MCP_CNF1,0xc1);
    MCP_write(MCP_CNF2,0xAC);
    MCP_write(MCP_CNF3,0x07);
    MCP_write(MCP_RXF0SIDH,MY_ADDR>>3);
    MCP_write(MCP_RXF0SIDL,MY_ADDR<<5);
    MCP_write(MCP_RXF1SIDH,MY_ADDR2>>3);
    MCP_write(MCP_RXF1SIDL,MY_ADDR2<<5);
    MCP_write(MCP_RXM0SIDH,0xFF);
    MCP_write(MCP_RXM0SIDL,0xE0);
    MCP_write(MCP_RXM0EID0,0);
    MCP_write(MCP_RXM0EID8,0);
    MCP_write(MCP_RXM1SIDH,0xFF);
    MCP_write(MCP_RXM1SIDL,0xE0);
    MCP_write(MCP_RXM1EID0,0);
    MCP_write(MCP_RXM1EID8,0);
    MCP_write(MCP_RXF2SIDH,0);
    MCP_write(MCP_RXF2SIDL,0x0);
    MCP_write(MCP_RXF3SIDH,0);
    MCP_write(MCP_RXF3SIDL,0x0);
    MCP_write(MCP_RXF4SIDH,0);
    MCP_write(MCP_RXF4SIDL,0x0);
    MCP_write(MCP_RXF5SIDH,0);
    MCP_write(MCP_RXF5SIDL,0x0);
    
    
    // Configure RX0 Buffer
    MCP_write(MCP_RXB0CTRL,0x24);
    MCP_write(MCP_RXB1CTRL,0x20);
    
    // configure TX Buffer
    MCP_write(MCP_CANCTRL,0);
    //Disable All RxNBF Pin
    MCP_write(MCP_RXnBF,0x00);
    //Enable Interrupt for RX0 RX1
    MCP_write(MCP_CANINTE,0x3);
    //Reset Error Count
    MCP_write(MCP_TEC,0);
    MCP_write(MCP_REC,0);
    //Clear All Interrupt
    MCP_write(MCP_CANINTF,0);
    cc_setMode(MODE_NORMAL);
    //Set POST SCALE
    MCP_write(MCP_CANCTRL,MODE_NORMAL|CLKOUT_ENABLE|PS);
}

void MCP_write(uint8_t addr,uint8_t data){
    uint8_t _data[3]={MCP_WRITE,DUMMY_DATA,DUMMY_DATA};
    _data[1]=addr;
    _data[2]=data;
    SPI_Exchange8bitBuffer(_data,3,_data);
}
void cc_setMode(uint8_t mode){
    uint8_t data[4]={MCP_BITMOD,MCP_CANCTRL,0xE0,DUMMY_DATA};
    data[3] = mode;
    SPI_Exchange8bitBuffer(data,4,data);
}
CAN_Data cc_readRxBn(RXBn buffer){
    uint8_t _data[14];
    CAN_Data return_val;
    initialize_array(_data,14);
    _data[0] = buffer;
    SPI_Exchange8bitBuffer(_data,14,_data);
    return_val.node_addr = ((_data[1] & 0xFF) << 3) | ((_data[2] & 0xE0) >> 5);
    return_val.RTR = (_data[5] & 0x40) >> 6;
    return_val.len = _data[5] & 0xF;
    for (int i=0; i< return_val.len ; i++){
        return_val.data[i] = _data[6+i];
    }
    return return_val;
}
void cc_LoadTx(CAN_Data packet){
    uint8_t _data[14];
    uint8_t buffer = cc_check_tx_empty();
    switch(buffer){
        case 0:
            _data[0] = MCP_LOAD_TX0;
            break;
        case 1:
            _data[0] = MCP_LOAD_TX1;
            break;
        case 2:
            _data[0] = MCP_LOAD_TX2;
            break;
        default:
            return;
    }
    _data[1] = (packet.node_addr & 0x7FF) >> 3;
    _data[2] = (packet.node_addr & 0x7) << 5;
    _data[3] = 0;
    _data[4] = 0;
    _data[5] = (packet.RTR <<6)| (packet.len & 0xF);
    for (int i=0; i < packet.len; i++){
        _data[6+i]=packet.data[i];
    }
    SPI_Exchange8bitBuffer(_data,packet.len+6,_data);
        switch(buffer){
        case 0:
            _data[0] = MCP_RTS_TX0;
            break;
        case 1:
            _data[0] = MCP_RTS_TX1;
            break;
        case 2:
            _data[0] = MCP_RTS_TX2;
            break;
    }
    SPI_Exchange8bitBuffer(_data,1,_data);
}

uint8_t cc_check_tx_empty(){
    uint8_t tmp[2];
    uint8_t empty_buffer = 0xff;

    tmp[0] = MCP_READ_STATUS;
    tmp[1] = DUMMY_DATA;
    SPI_Exchange8bitBuffer(tmp,2,tmp);
    if ((tmp[1] & 0x4)==0)
        empty_buffer = 0;
    else if((tmp[1] & 0x8) == 0)
        empty_buffer = 1;
    else if ((tmp[1] & 0x10) == 0)
        empty_buffer = 2;

    return empty_buffer;
}

void initialize_array(uint8_t arry[],uint8_t len){
    int i=0;
    for(i=0;i<len;i++){
        arry[i]=DUMMY_DATA;
    }
}

uint8_t get_buffer_ready(){
    uint8_t data[2]={MCP_RX_STATUS,DUMMY_DATA};
    SPI_Exchange8bitBuffer(data,2,data);
    return (data[1] & 0xC0) >> 6;
}