/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using MPLAB(c) Code Configurator

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB(c) Code Configurator - 3.16
        Device            :  PIC16F1716
        Driver Version    :  2.00
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.35
        MPLAB             :  MPLAB X 3.20
*/
/* ERROR CODE:
 * +-----+---------------------------------------------------------------------+
 * | BIT | Description                                                         |
 * +-----+---------------------------------------------------------------------+
 * |  0  | Encoder Error
 * +-----+---------------------------------------------------------------------+
 */

#include "mcc_generated_files/mcc.h"
#include "bareMetal.h"
#include "PID.h"

#define MY_ADDR 0x2
#define MY_ADDR2 0x0

#define ABS(x) ((x) > 0 ? (x) : -(x))

//Steering Use
int mvel,UpCount,DnCount = 0;

bool en_PID = false;
bool isMoving = false;
void read_encoder();
void ResetEnc();

long TargetEncoder = 0;

void RP_Steering();
void encoder_Check();


int map(long x, long in_min, long in_max, int out_min, int out_max);


bool ManualMode = false;
CAN_Data Report_Data;
int *encoder_count = 0;
int prev_encoder_count =0;
uint8_t *status = 0;
int16_t *current_DAC;
uint8_t horn_time = 0;
void device_specific_command(CAN_Data buff);
void find_center();
void horn();

void putStr(char *txStr)
{
    while (*txStr != 0)
    {
        putch(*txStr);
        txStr++;
    }
}

void uartPrintVersion()
{
    char version[16];
    sprintf(version, "%s", "Steer v0.4.0\n\r");
    putStr(version);
}

void main(void){
    SYSTEM_Initialize();
    DAC1_SetOutput(127);
    init_bareMetal(MY_ADDR,MY_ADDR2);
    Report_data_ptr = &Report_Data;
    status = &Report_Data.data[1];
    encoder_count = &Report_Data.data[2];
    current_DAC = &Report_Data.data[4];
    Report_Data.len = 6;
    SetCommandCallBack(device_specific_command);
    
    init_PID(MY_ADDR);
    
    register_thread(80,encoder_Check);
    register_thread(EEDATA[PID_DURATION],read_encoder);
    register_thread(EEDATA[HORN_DURATION],horn);
    
    //C4_SetHigh();
    //Manual_SetLow();
    //__delay_ms(1500);
    //C4_SetLow();

    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();
    SWDTEN = 1;
    
    //uartPrintVersion();
        
    while (1)
    {
        thread_update();
        Received_Command();
        
        //uartPrintVersion();
    }
}
void read_encoder(){
    int out_val;
    mvel = DnCount;
    mvel -= UpCount;	
    UpCount = (TMR0_H<<8)|TMR0_ReadTimer();
    DnCount = TMR1_ReadTimer();
    // Add new count values into result.
    mvel += UpCount;
    mvel -= DnCount;
    // Add measured velocity to measured position to get new motor
    // measured position.
    *encoder_count -= mvel;
   
    if (en_PID){
        out_val = EEDATA[CENTER_DAC] + (int)((float)((float)TargetEncoder/(float)12000)*65);
        //out_val = do_PID(*encoder_count,TargetEncoder);
        DAC1_SetOutput(out_val); 
    }
    *current_DAC = DAC1CON1;
}

void encoder_Check(){
    if (HomeSensor_GetValue()==1){
        ResetEnc();
    }
    if(ABS(prev_encoder_count - *encoder_count)<EEDATA[ENC_THRESHOLD])
        isMoving = 0;
    else
        isMoving = 1;
    prev_encoder_count=*encoder_count;
    
    ManualMode = Manual_GetValue();
    *status = ManualMode<<1|isMoving;
    if (*current_DAC != EEDATA[CENTER_DAC] && *encoder_count == 0){
        error_code = 0x1;
    } else
        error_code = 0;
}

void ResetEnc(){  
    if (DAC1CON1 == EEDATA[CENTER_DAC]){
        mvel = 0;
        TMR0_H =0;
        TMR0_WriteTimer(0);
        TMR1_WriteTimer(0);
        UpCount = 0;
        DnCount = 0;
        *encoder_count=0;
        en_PID = false;
    }
}

void find_center(){
    unsigned char low_offset = 0, high_offset = 0;
    bool reach = 0;
    int center[3] = {0,0,0}; 
    int init_val = 90;
    SWDTEN = 0;
    for (int j = 0; j < 3; j++){
        DAC1_SetOutput(init_val);
        __delay_ms(2000);
        for (int i = init_val; i <200; i++){
            DAC1_SetOutput(i);
            __delay_ms(500);
            if (HomeSensor_GetValue()== 1 && reach == 0){
                low_offset = i;
                reach = 1;
            }
            if (HomeSensor_GetValue()== 0 && reach == 1){
                high_offset = i;
                reach = 0;
                i = 200;
            }
        }
        center[j] = ((int)low_offset + (int)high_offset)>> 1;
    }
    EEDATA[CENTER_DAC] = (center[0] + center[1] + center[2])/3;
    write_EEPROM(CENTER_DAC,EEDATA[CENTER_DAC]);
    __delay_ms(500);
    DAC1_SetOutput(EEDATA[CENTER_DAC]);
    SWDTEN = 0;
}
void device_specific_command(CAN_Data buff){
    int out_val;
    switch(buff.data[0]){
        case 0:
            out_val = (buff.data[1]);            
            DAC1_SetOutput(out_val);             
            break;
        case 1:
            en_PID = false;
            *encoder_count = buff.data[1]<<8 | buff.data[2];
            break;
        case 2:
            en_PID = false;
            DAC1_SetOutput(EEDATA[CENTER_DAC]);
            break;
        case 3:
            TargetEncoder =(((buff.data[1])<<8)|(buff.data[2]));
            en_PID = true;
            if (TargetEncoder == 0){
                en_PID = false;
                DAC1_SetOutput(EEDATA[CENTER_DAC]);
                reset_PID();
            }
            break;
        case 4:
            horn_time = buff.data[1] * 2;
            break;
        case 5:
            Manual_LAT = buff.data[1];
            break;
        case 6:
            DAC1_SetOutput(127);
            __delay_ms(2000);
            HomeReset_SetHigh();
            C4_SetHigh();
            __delay_ms(1000);
            C4_SetLow();
            __delay_ms(1000);
            HomeReset_SetLow();
            C4_SetHigh();
            __delay_ms(1000);
            C4_SetLow();
            break;
        case 80:
            debug_PID = buff.data[1];
            break;
        case 100:
            find_center();
            break;
    }
}

void horn(){
    
    if(horn_time >0){
        Horn_Toggle();
        horn_time--;      
    }
    else{
        Horn_SetLow();
    }
}
//end