#include "PID.h"

void init_PID(int addr){
    MYADDR = addr;
}

int do_PID(int val, int target){
    long error = target>>4;
    long d_error = error - p_error;
    int ret_val = 0;
    i_error += (target-val)>>6;
    p_error = error;
    
    long p_term = ((long)EEDATA[KP] * error);
    long i_term = ((long)EEDATA[KI] * i_error);
    long d_term = ((long)EEDATA[KD] * d_error);
    ret_val = (long)((p_term + i_term + d_term))>>10;
    if(ret_val > EEDATA[PID_MAX]){
        ret_val = EEDATA[PID_MAX];
        i_error -= error;
    }
    if (ret_val < EEDATA[PID_MIN]){
        ret_val = EEDATA[PID_MIN];
        i_error -= error;
    }
    return ret_val;
}

void reset_PID(){
    i_error = 0;
    p_error = 0;
}
