#include "PID.h"

void init_PID(int addr){
    MYADDR = addr;
}
int do_PID(int val, int target){
    CAN_Data debug_data;
    int error = target - val;
    int d_error = error - p_error;
    int ret_val = 0;
    p_error = error;
    
    long p_term = (EEDATA[KP] * error)/EEDATA[PID_FACTOR];
    long i_term = (EEDATA[KI] * i_error)/EEDATA[PID_FACTOR];
    long d_term = (EEDATA[KD] * d_error)/EEDATA[PID_FACTOR];
    ret_val = (p_term + i_term - d_term)/EEDATA[PID_OUT_FACTOR];
    
    if(ret_val > EEDATA[PID_MAX]){
        ret_val = EEDATA[PID_MAX];
        i_error -= error;
    }
    if (ret_val < EEDATA[PID_MIN]){
        ret_val = EEDATA[PID_MIN];
        i_error -= error;
    }
    
    i_error += error;
    if(debug_PID){
        debug_data.RTR = 0;
        debug_data.node_addr = (1<<10)|MYADDR;
        debug_data.data[0]=0x20;
        debug_data.data[1]=p_term;
        debug_data.data[2]=p_term>>8;
        debug_data.data[3]=i_term;
        debug_data.data[4]=i_term>>8;
        debug_data.data[5]=d_term;
        debug_data.data[6]=d_term>>8;
        debug_data.len = 7;
        cc_LoadTx(debug_data);
         debug_data.data[0]=0x21;
        debug_data.data[1]=ret_val;
        debug_data.data[2]=ret_val>>8;
        debug_data.len = 3;
        cc_LoadTx(debug_data);
    }
    return ret_val;
}

void reset_PID(){
    i_error = 0;
    p_error = 0;
}
