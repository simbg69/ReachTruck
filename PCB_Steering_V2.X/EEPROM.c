/*
 * File:   EEPROM.c
 * Author: Jonathan Chang
 *
 * Created on March 29, 2017, 10:47 AM
 */


#include "EEPROM.h"

int read_EEPROM(uint8_t addr){
    return FLASH_ReadWord(EEPROM_ADDR +1 +addr*2)<<8|FLASH_ReadWord(EEPROM_ADDR+addr*2);
}

void write_EEPROM(uint8_t EEPROMAddr, int word)
{
    uint16_t flashAddr = EEPROM_ADDR + EEPROMAddr*2;
    uint16_t blockStartAddr = (uint16_t)(flashAddr & ((END_FLASH-1) ^ (ERASE_FLASH_BLOCKSIZE-1)));
    uint8_t offset = (uint8_t)(flashAddr & (ERASE_FLASH_BLOCKSIZE-1));
    uint8_t i;
    uint16_t ramBuf[ERASE_FLASH_BLOCKSIZE];

    // Entire row will be erased, read and save the existing data
    for (i=0; i<ERASE_FLASH_BLOCKSIZE; i++)
    {
        ramBuf[i] = FLASH_ReadWord((blockStartAddr+i));
    }

    // Write at offset
    ramBuf[offset] = word;
    ramBuf[offset+1] = word>>8;

    // Writes ramBuf to current block
    FLASH_WriteBlock(blockStartAddr, ramBuf);
}

void restore_EEData(){
    for (int i = 0 ; i< MAX_EE_DATA; i++){
        EEDATA[i] = read_EEPROM(i);
    }
}